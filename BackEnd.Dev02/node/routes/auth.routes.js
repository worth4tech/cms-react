const AUTH_URL = "http://auth-chatly-staging.azurewebsites.net/";
const router = require('express').Router();
const axios = require('axios');

router.post('/*', (req, res) => {
  let url = req.path;
  axios.post(AUTH_URL + req.path, req.body).then((success) => {
    res.status(success.status).json({
      "message": "",
      "result": success.data
    })
  }).catch((error) => {
    res.status(error.response.status).json({
      "message": error.response.message,
      "result": error.response.data
    })
  })
});

module.exports = router;
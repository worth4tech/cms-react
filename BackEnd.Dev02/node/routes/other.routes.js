//const API_URL = "http://cmsdataapi.azurewebsites.net";
const API_URL = "http://cmsapisoutheastasia.azurewebsites.net";
const router = require('express').Router();
const axios = require('axios');

router.get('/*', (req, res) => {
  let headers = {
    headers: {
      "Authorization": req.headers['authorization'],
      "Account_SID": req.headers['account_sid'],
      "ServiceBin_SID": req.headers['servicebin_sid']
    }
  }

  let queryString = JSON.stringify(req.query) !== '{}' ? '?' + Object.keys(req.query).map(key => key + '=' + req.query[key]).join('&') : '';

  let url = API_URL + req.path + queryString;

  axios.get(url, headers).then((success) => {

    res.status(success.status).json({
      "message": success.data.message,
      "results": success.data
    })
  }).catch((error) => {
    if (error.response) {
      res.status(error.response.status).json({
        "message": error.response.message,
        "results": error.response.data
      })
    }
  })
});

router.post('/*', (req, res) => {
  let headers = {
    headers: {
      "Authorization": req.headers['authorization'],
      "Account_SID": req.headers['account_sid'],
      "ServiceBin_SID": req.headers['servicebin_sid']
    }
  }
  if (req.headers['content_type_sid']) {
    headers.headers['content_type_sid'] = req.headers['content_type_sid'];
  }
  if (req.headers['chat_account_sid']) {
    headers.headers['chat_account_sid'] = req.headers['chat_account_sid'];
  }
  let queryString = JSON.stringify(req.query) !== '{}' ? '?' + Object.keys(req.query).map(key => key + '=' + req.query[key]).join('&') : '';

  let url = API_URL + req.path + queryString;

  axios.post(url, req.body, headers).then((success) => {

    res.status(success.status).json({
      "message": "",
      "results": success.data
    })
  }).catch((error) => {
    if (error.response) {
      res.status(error.response.status).json({
        "message": "",
        "results": error.response.data
      })
    }

  })

});

router.put('/*', (req, res) => {
  res.status(200).json({
    "message": "Put method"
  })
});

router.delete('/*', (req, res) => {
  let headers = {
    headers: {
      "Authorization": req.headers['authorization'],
      "Account_SID": req.headers['account_sid'],
      "ServiceBin_SID": req.headers['servicebin_sid']
    }
  }
  if (req.headers['chat_account_sid']) {
    headers.headers['chat_account_sid'] = req.headers['chat_account_sid'];
  }
  let url = API_URL + req.path;

  axios.delete(url, headers).then((success) => {
    res.status(success.status).json({
      "message": success.data.message,
      "results": success.data.results
    })
  }).catch((error) => {
    res.status(error.response.status).json({
      "message": error.response.message,
      "results": error.response.data
    })
  })
});

module.exports = router;
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

const authRoutes = require('./routes/auth.routes');
const otherApiRoutes = require('./routes/other.routes');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'GET, POST, OPTIONS, PUT, PATCH, DELETE,content-type,Authorization,ServiceBin_SID,account_SID,content_type_sid,chat_account_sid');
  next();
});

app.use('/api/user', authRoutes);
app.use('/api/other', otherApiRoutes);

app.get('/status', (req, res) => {
  res.status(200).json({
    "message": "Server is running"
  })
});

app.listen(3000, () => {
  console.log("Server is listening on port 3000");
});

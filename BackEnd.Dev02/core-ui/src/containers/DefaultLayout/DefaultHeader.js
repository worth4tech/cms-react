import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import {
  //Badge, Button,
  UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem
} from 'reactstrap';
import PropTypes from 'prop-types';
import {
  //AppAsideToggler, 
  AppNavbarBrand, AppSidebarToggler
} from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg'
import sygnet from '../../assets/img/brand/sygnet.svg'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: localStorage.getItem('first_name') + ' ' + localStorage.getItem('last_name')
    }
  }
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
    return (
      <React.Fragment>
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'CMS' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CMS' }}
        />
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="icon-wrapper" navbar>
          <NavItem className="px-3">
            <NavLink to="/content-type" className="nav-link">
              <i className="fas fa-cubes fa-2x"></i> <span className="nav-title">Content Model</span></NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/content-entries" className="nav-link"><i className="far fa-file-alt fa-2x"></i> <span className="nav-title">Content</span></Link>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/assets" className="nav-link"><i className="far fa-file-alt fa-2x"></i> <span className="nav-title">Assets</span></Link>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="#" className="nav-link"><i className="fa fa-cogs fa-2x"></i> <span className="nav-title">Settings</span></NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          {/*<NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="cui-speech icons font-2xl"></i><Badge pill color="danger">5</Badge></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none border-right">
            <NavLink to="#" className="nav-link">
              <Button color="menu-btn">Menus</Button>
            </NavLink>

          </NavItem>
           <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="cui-cog icons font-2xl d-block"></i></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="fa fa-book font-2xl"></i></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none border-right">
            <NavLink to="#" className="nav-link"><i className="fa fa-question font-2xl"></i></NavLink>
          </NavItem> */}
          <UncontrolledDropdown nav direction="down" className="header-avatar">
            <DropdownToggle nav className="dropdown-toggle mr-1">
              <img src={'../../assets/img/avatars/6.jpg'} className="img-avatar" alt="" />
              {this.state.userName}
            </DropdownToggle>
            <DropdownMenu right style={{ "position": "absolute", "willChange": "transform", "top": "0px", "left": "0px", "transform": "translate3d(43px, 58px, 0px)" }}>
              <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
              {/*  <DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge color="info">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge color="success">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge></DropdownItem> 
              <DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>*/}
              <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
              {/*<DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
               <DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge></DropdownItem>
              <DropdownItem divider />
              <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem> */}
              <DropdownItem onClick={e => this.props.onLogout(e)}><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        {/* <AppAsideToggler className="d-md-down-none" />
        <AppAsideToggler className="d-lg-none" mobile /> */}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;

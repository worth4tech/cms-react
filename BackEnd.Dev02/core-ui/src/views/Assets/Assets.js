import React from 'react';
import {
  /* Card,
  CardBody, Button,*/
  FormGroup,
  Row, Col, Button,
 } from 'reactstrap'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, { PaginationProvider } from 'react-bootstrap-table2-paginator';
import APIService from '../../services/APIService';

const TableWrapper = ({ data, columns, onTableChange, pagination }) => (
  <React.Fragment>
    <PaginationProvider
      pagination={
        paginationFactory({
          custom: true,
          page: pagination.Page,
          sizePerPage: pagination.PageSize
        })
      }
    >
      {
        ({
          paginationProps,
          paginationTableProps
        }) => (
            <React.Fragment>
              <BootstrapTable
                remote
                keyField="asset_sid"
                data={data}
                options={{ noDataText: "No entry(s) found" }}
                columns={columns}
                onTableChange={onTableChange}
                {...paginationTableProps}
                defaultSorted={[{
                  dataField: 'name',
                  order: 'asc'
                }]}
                hover responsive bootstrap4
              >
              </BootstrapTable>
              <div role="group" className="float-right mb-1">
                <Button color="link" disabled={pagination.page === 1 ||
                  data.length < pagination.sizePerPage ? true : false}><i className="fas fa-chevron-circle-left fa-2x"></i></Button>

                <Button color="link" className="ml-1"
                  disabled={data.length < pagination.row ? true : false}
                  onClick={() => { onTableChange('', { ...pagination, "page": ++pagination.page }) }}><i className="fas fa-chevron-circle-right fa-2x"></i></Button>
              </div>
            </React.Fragment>
          )
      }
    </PaginationProvider>
  </React.Fragment>
);

class Assets extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      pagination: {
        page: '1',
        sizePerPage: '10',
        sortField: 'last_modified_datetime',
        sortOrder: 'desc',
        searchText: ''
      },
      "columns": [
        {
          "text": "Name",
          "dataField": "name",
          "sort": true,
          formatter: (cell, row, rowIndex, extraData) => (
            <a className="btn btn-link" href={row.file_uri}  rel="noopener noreferrer" target="_blank"> {row.name}</a>
          ),
        },
        {
          "text": "Description",
          "dataField": "description",
          "sort": true
        },
        {
          "text": "Status",
          "sort": true,
          "dataField": "asset_status",
        },
        {
          "text": "Action(s)",
          "dataField": 'action',
          "isDummyField": true,
          "sort": false,
          formatter: (cell, row, rowIndex, extraData) => (
            <Link className="btn btn-link" to={row.contenttype_sid + "/content-entries/" + row.entry_sid + '/detail'}> Details</Link >
          ),
        }
      ],
      assets: [],
    }
    this.getAssets = this.getAssets.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    let pagination = this.state.pagination;
    this.getAssets('', pagination);
  }
  componentWillUnmount() { this._isMounted = false; }

  getAssets(type = '', pagination) {

    let accountId = localStorage.getItem('account_sid');
    let url = 'other/RCMS.Data/v1/Accounts/' + accountId + '/Assets?Page=' + pagination.page + '&PageSize=' + pagination.sizePerPage + '&SortColumn=' + pagination.sortField + '&SortOrder=' + pagination.sortOrder

    this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });

    APIService.get(url).then((success) => {
      if (success.status === 200) {
        let assets = { assets: success.data.results.results }
        this.setState(assets);
      }
    }).catch((error) => {
      console.log(error)
    }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })
  }

  render() {

    return (
      <React.Fragment>
        <Row className="mt-1">
          <Col md={5} className="offset-3">
            {/* <div className="form-inline">
              <FormGroup className="mr-1" >
                <InputGroup>
                  <Input name="searchText" id="searchText" onChange={this.handleInput} value={pagination.searchText}
                    placeholder="search" />
                  <InputGroupButtonDropdown addonType="append" isOpen={this.state.isDropdownSearch} toggle={this.toggleContentTypeDropdown}>
                    <DropdownToggle caret>
                      Button Dropdown
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem header>Header</DropdownItem>
                      <DropdownItem disabled>Action</DropdownItem>
                      <DropdownItem>Another Action</DropdownItem>
                      <DropdownItem divider />
                      <DropdownItem>Another Action</DropdownItem>
                    </DropdownMenu>
                  </InputGroupButtonDropdown>
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <Button color="primary" ><i className="fa fa-search"></i>Search</Button>
              </FormGroup>
            </div> */}
          </Col>
          <Col md={4} className="form-inline">
            <FormGroup>
              <Button color="primary"  onClick={() => {
                this.props.history.push('assets/add-new/detail')
              }}>
                Add Assets</Button>
            </FormGroup>
          </Col>
        </Row>
        <Row className="mt-2">
          <TableWrapper
            data={this.state.assets}
            columns={this.state.columns}
            onTableChange={this.getAssets}
            pagination={this.state.pagination}
          //selectRow={this.handleRowSelection}
          />
        </Row>
      </React.Fragment>
    );
  }
}

export default connect((state) => {
  return { isLoader: state.isLoader }
})(Assets);

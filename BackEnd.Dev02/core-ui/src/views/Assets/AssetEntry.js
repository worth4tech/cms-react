import React from 'react';
import { Button, Label, Card, CardBody, CardGroup, Col, Input, FormGroup, Row } from 'reactstrap';
import { connect } from 'react-redux';
import APIService from '../../services/APIService';

class AssetEntry extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props)
    this.state = {
      asset: {},
      errros: {},
      submitted: false
    }
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(event) {
    let asset = this.state.asset;
    asset[event.target.id] = event.target.value;
    this.setState({ asset: asset })
  }

  render() {
    let errors = this.state.errros;
    let asset = this.state.asset;
    let submitted = this.state.submitted;
    return (
      <React.Fragment>
        <Row className="justify-content-center mt-1">
          <Col md="9">
            <CardGroup>
              <Card className="p-4">
                <CardBody>
                  <h1>Add Asset</h1>
                  <p className="text-muted">Add Asset</p>
                  <FormGroup className="mb-3">
                    <Label for="emailaddress"> Name <span className="invalid-feedback">*</span></Label>
                    <Input type="email" placeholder="Enter name" id="emailaddress" name="emailaddress" value={asset.name} onChange={this.handleInput} ref="emailaddress" className={submitted && errors.emailaddress ? 'is-invalid' : ''} />

                    {submitted && errors.name &&
                      <div className="invalid-feedback">{errors.name}</div>
                    }
                  </FormGroup>
                  <FormGroup className="mb-4">
                    <Label for="password">Description <span className="invalid-feedback">*</span></Label>
                    <Input type="textarea" placeholder="Enter description" name="description" value={asset.desription} onChange={this.handleInput} id="description" 
                      className={submitted && errors.description ? 'is-invalid' : ''} />
                    {submitted && errors.description &&
                      <div className="invalid-feedback">{errors.description}</div>
                    }
                  </FormGroup>
                  <Row>
                    <Col xs="6">

                      <Button color="primary" type="submit" className="mr-1">
                        Submit
                        </Button>
                      <Button color="danger" type="reset">
                        Reset
                        </Button>

                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </CardGroup>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}
export default connect((state) => {
  return { isLoader: state.isLoader }
})(AssetEntry);

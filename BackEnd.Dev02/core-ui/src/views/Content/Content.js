import React from 'react';
import { Link } from 'react-router-dom';
import {
  /* Card,
  CardBody, Button,*/
  DropdownToggle,
  DropdownMenu, DropdownItem,
  //ButtonDropdown,
  FormGroup, InputGroup, InputGroupButtonDropdown, Input,
  Row, Col, Button
} from 'reactstrap';
import { connect } from 'react-redux';
import moment from 'moment';
import BootstrapTable from 'react-bootstrap-table-next';
import Select from 'react-select';
import paginationFactory, { PaginationProvider } from 'react-bootstrap-table2-paginator'
//import { toast } from 'react-toastify';
import APIService from '../../services/APIService';

const TableWrapper = ({ data, columns, onTableChange, pagination }) => (
  <React.Fragment>
    <PaginationProvider
      pagination={
        paginationFactory({
          custom: true,
          page: pagination.page,
          sizePerPage: pagination.sizePerPage
        })
      }
    >
      {
        ({
          paginationProps,
          paginationTableProps
        }) => (
            <React.Fragment>
              <BootstrapTable
                remote
                keyField="entry_sid"
                data={data}
                options={{ noDataText: "No entry(s) found" }}
                columns={columns}
                onTableChange={onTableChange}
                { ...paginationTableProps }
                defaultSorted={[{
                  dataField: 'last_modified_datetime',
                  order: 'desc'
                }]}
                hover responsive bootstrap4
              >
              </BootstrapTable>
              <div role="group" className="float-right mb-1">
                <Button color="link" disabled={pagination.page === 1 ||
                  data.length < pagination.sizePerPage ? true : false}><i className="fas fa-chevron-circle-left fa-2x"></i></Button>

                <Button color="link" className="ml-1"
                  disabled={data.length < pagination.sizePerPage ? true : false}
                  onClick={() => { onTableChange('', { ...pagination, "page": ++pagination.page }) }}><i className="fas fa-chevron-circle-right fa-2x"></i></Button>
              </div>
            </React.Fragment>
          )
      }
    </PaginationProvider>
  </React.Fragment>
);

class Content extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      pagination: {
        page: 1,
        sizePerPage: 1000,
        sortField: 'last_modified_datetime',
        sortOrder: 'desc',
        searchText: ''
      },
      isDropdownSearch: false,
      isDropdownAdd: false,
      "columns": [
        {
          "text": "Title",
          "dataField": "entry_title",
          "sort": true,
          "formatter": (cell, row, rowIndex, formatExtraData) => {
            return row.entry_title ? row.entry_title + "  " + row.entry_sid : 'Untitled' + "  " + row.entry_sid;
          }
        },
        {
          "text": "Status",
          "dataField": "entry_status",
          "sort": true
        },
        {
          "text": "Updated At",
          "sort": true,
          "dataField": "last_modified_datetime",
          "formatter": (cell, row, rowIndex, formatExtraData) => {
            return row.last_modified_datetime !== '' ?
              moment(new Date(row.last_modified_datetime)).fromNow()
              : '';
          }
        },
        {
          "text": "Action(s)",
          "dataField": 'action',
          "isDummyField": true,
          "sort": false,
          formatter: (cell, row, rowIndex, extraData) => (
            <Link className="btn btn-link" to={row.contenttype_sid + "/content-entries/" + row.entry_sid + '/detail'}> Details</Link >
          ),
        }
      ],
      contentTypeEntries: [],
      contentTypes: [],
      contentType: null
    }
    this.getEntries = this.getEntries.bind(this);
    this.getContentTypes = this.getContentTypes.bind(this);
    this.toggleContentTypeDropdown = this.toggleContentTypeDropdown.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSelection = this.handleSelection.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    let pagination = this.state.pagination;
    this.getEntries('', pagination);
    this.getContentTypes();
  }

  componentWillUnmount() { this._isMounted = false; }

  getEntries(type = '', pagination) {

    let accountId = localStorage.getItem('account_sid');
    let url = 'other/RCMS.Data/v1/Accounts/' + accountId + '/Entries?Page=' + pagination.page + '&PageSize=' + pagination.sizePerPage + '&SortColumn=' + pagination.sortField + '&SortOrder=' + pagination.sortOrder

    this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
    APIService.get(url).then((success) => {
      if (success.status === 200) {
        let entries = { contentTypeEntries: success.data.results.results }
        this.setState(entries);
      }
    }).catch((error) => { console.log(error) })
      .finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })

  }

  getContentTypes() {

    let accountId = localStorage.getItem('account_sid');
    let url = 'other/RCMS.Data/v1/Accounts/' + accountId + '/ContentTypes?SearchText=&Page=1&PageSize=1000&SortColumn=name&SortOrder=desc&Filters&FiltersList'

    APIService.get(url).then((success) => {
      if (success.status === 200) {
        let contentTypes = { contentTypes: success.data.results.results }
        this.setState(contentTypes);
      }
    }).catch((error) => { console.log(error) })
  }

  handleInput(event) {
    if (event.target.type === 'text') {
      let pagination = this.state.pagination;
      pagination[event.target.id] = event.target.value;
      this.setState({ pagination: pagination })
    }
  }
  handleSelection(selectedOptions) {
    this.setState({ contentType: selectedOptions });
  }
  toggleContentTypeDropdown(type = 'search') {
    if (type === 'add') {
      this.setState({
        isDropdownAdd: !this.state.isDropdownAdd
      });
    }
    else {
      this.setState({
        isDropdownSearch: !this.state.isDropdownSearch
      });
    }
  }

  render() {
    let pagination = this.state.pagination;
    let contentTypes = JSON.stringify(this.state.contentTypes);
    contentTypes = contentTypes.split('name').join('label').split('contenttype_sid').join('value');
    contentTypes = JSON.parse(contentTypes).map((contentType) => {
      return { key: contentType.value, 'value': contentType.value, 'label': contentType.label };
    });
    return (
      <React.Fragment>
        <Row className="mt-1">
          <Col md={5} className="offset-3">
            <div className="form-inline">
              <FormGroup className="mr-1" >
                <InputGroup>
                  <Input name="searchText" id="searchText" onChange={this.handleInput} value={pagination.searchText}
                    placeholder="search" />
                  <InputGroupButtonDropdown addonType="append" isOpen={this.state.isDropdownSearch} toggle={this.toggleContentTypeDropdown}>
                    <DropdownToggle caret>
                      Button Dropdown
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem header>Header</DropdownItem>
                      <DropdownItem disabled>Action</DropdownItem>
                      <DropdownItem>Another Action</DropdownItem>
                      <DropdownItem divider />
                      <DropdownItem>Another Action</DropdownItem>
                    </DropdownMenu>
                  </InputGroupButtonDropdown>
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <Button color="primary" ><i className="fa fa-search"></i>Search</Button>
              </FormGroup>
            </div>
          </Col>
          <Col md={4} className="form-inline">
            <FormGroup>
              <Select
                value={this.state.contentType}
                onChange={this.handleSelection}
                options={contentTypes}
                className="content-type-select"
                placeholder="Select Content Type"
                isSearchable={true}
              />
              {/*<Input type="select" name="contentTypeDropdown" id="contentTypeDropdown" defaultValue={this.state.contentType} onChange={this.handleInput}>
                
              </Input>
               <ButtonDropdown isOpen={this.state.isDropdownAdd} toggle={() => this.toggleContentTypeDropdown('add')} className="brl-0">
                <DropdownToggle caret>
                  Select Content Type
                </DropdownToggle>
                <DropdownMenu>
                  {contentTypeDropdownItems}
                </DropdownMenu>
              </ButtonDropdown> */}
              <Button color="primary" className="blr-0" disabled={this.state.contentType === null ? true : false} onClick={() => {
                this.props.history.push(this.state.contentType.value + '/content-entries/add-new/detail')
              }}>
                Add Content Entry</Button>

            </FormGroup>
          </Col>
        </Row>
        <Row className="mt-2">
          <Col md={3} className="border">Sidebar</Col>
          <Col md={9}>
            <TableWrapper
              data={this.state.contentTypeEntries}
              columns={this.state.columns}
              onTableChange={this.getEntries}
              pagination={pagination}
            //selectRow={this.handleRowSelection}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default connect((state) => {
  return { isLoader: state.isLoader }
})(Content);

import React from 'react';
import {
  /* Card,
  CardBody, Button,*/
  ButtonDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  FormGroup,
  Label,
  Row, Col, Button,
  Modal, ModalHeader,
  ModalBody, ModalFooter
} from 'reactstrap';
import Select from 'react-select';
import ContentField from '../../components/ContentField';
import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import APIService from '../../services/APIService';

class ContentEntry extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      contentType: {},
      labels: [],
      entry: {},
      selectedLabels: [],
      selectedLabelSid: [],
      isDropdown: false,
      isStatusDropdown: false,
      localInfo: {},
      actionText: 'Action',
      isDeleteConfirm: false
    }
    this.getContentType = this.getContentType.bind(this);
    this.getLocals = this.getLocals.bind(this);
    this.getLabels = this.getLabels.bind(this);
    this.handleFormInput = this.handleFormInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleValidation = this.handleValidation.bind(this);
    this.handleSelection = this.handleSelection.bind(this);
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.toggleStatusDropdown = this.toggleStatusDropdown.bind(this);

    this.toggleDeleteConfirmModal = this.toggleDeleteConfirmModal.bind(this);
    this.deleteEntry = this.deleteEntry.bind(this);
    this.deleteLabel = this.deleteLabel.bind(this);
    this.saveLabel = this.saveLabel.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getLocals();
    this.getLabels();
    if (this.props.match.params.entry_sid !== 'add-new') {
      let url = 'other/RCMS.Data/v1/Entries/' + this.props.match.params.entry_sid;
      APIService.get(url).then((success) => {
        if (success.status === 200) {
          let response = success.data.results;
          let contentType = { ...response.content_type_details };
          contentType['fields'] = response.field_values.map(fv => {
            return {
              'value': fv.value,
              'field_value_sid': fv.field_value_sid,
              ...fv.fields
            }
          });
          let selectedLabelSid = response.labels.map(rl => rl.label_sid);

          this.setState({
            'entry': { 'isAdd': false, ...response },
            'contentType': contentType,
            'selectedLabels': response.labels,
            'selectedLabelSid': selectedLabelSid
          });
        }

      }).catch((error) => {
        console.log(error);
      })
    }
    else {
      this.getContentType();
    }
  }
  componentWillUnmount() { this._isMounted = false; }

  getContentType() {

    let contentTypeSid = this.props.match.params.content_type_sid;

    localStorage.setItem('content_type_sid', contentTypeSid);
    let url = 'other/RCMS.Data/v1/ContentTypes/' + contentTypeSid;

    this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });

    APIService.get(url).then((success) => {
      if (success.status === 200) {

        let contentType = success.data.results;

        this.setState({ contentType: contentType });
      }
    }).catch((error) => {
      console.log(error)
    }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })
  }

  getLabels() {
    let accountSid = localStorage.getItem('account_sid');
    if (this.state.selectedLabels.length === 0) {
      let url = 'other/RCMS.Data/v1/Accounts/' + accountSid + '/Labels?Page=1&PageSize=1000&SortColumn=&SortOrder=';

      APIService.get(url).then((success) => {
        if (success.status === 200) {
          this.setState({ labels: success.data.results.results });
        }
      }).catch((error) => {
        console.log(error)
      })
    }
  }

  getLocals() {
    let url = 'other/RCMS.Data/v1/Accounts/' + localStorage.getItem('account_sid') + '/Locales?Page=1&PageSize=100&SortColumn=name&SortOrder=desc&Filters=[{"key":"isdefault","value":"true"}]';
    this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
    APIService.get(url).then((success) => {
      if (success.status === 200) {
        this.setState({
          localInfo: success.data.results.results[0]
        })
      }
    }).catch((error) => {
      console.log(error);
      //let message = error.response.data.results.message;
      //toast.error(message)
    }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })
  }

  toggleDropdown(event) {
    let actionText = event.target !== null ? event.target.value : 'Action';
    this.setState(prevState => ({
      actionText: actionText,
      isDropdown: !prevState.isDropdown
    }));
  }

  toggleStatusDropdown(event) {
    //let actionText = event.target !== null ? event.target.value : 'Action';
    this.setState(prevState => ({
      //actionText: actionText,
      isStatusDropdown: !prevState.isStatusDropdown
    }));
  }

  toggleDeleteConfirmModal() {
    this.setState(prevState => ({
      isDeleteConfirm: !prevState.isDeleteConfirm
    }));
  }

  deleteEntry() {
    this.toggleDeleteConfirmModal();
    this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
    let entrySid = this.props.match.params.entry_sid;
    let url = 'other/RCMS.Data/v1/Entries/' + entrySid;

    APIService.delete(url).then((success) => {
      if (success.status === 204) {
        this.props.history.push('/content-entries');
      }
    }).catch((error) => {
      let message = error.response.data.results.message;
      toast.error(message)
    }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })
  }

  handleFormInput(fieldConfiguation) {
    let contentType = this.state.contentType;
    contentType.fields[fieldConfiguation.index] = fieldConfiguation;

    contentType.fields.forEach((elField, index) => {
      if (elField.required && (!elField.value || elField.value === '')) {
        contentType.fields[index].message = elField.field_validations[elField.required.index].template_json.customerrormessage['defaultvalidationmessage'];
      }
      else if (elField.range && elField.value.length < elField.range.min) {
        contentType.fields[index].message = elField.field_validations[elField.range.index].template_json.customerrormessage['defaultvalidationmessage'];
      }
      else if (elField.pattern) {
        contentType.fields[index].message = elField.field_validations[elField.pattern.index].template_json.customerrormessage['defaultvalidationmessage'];
      }
      else if (elField.specificItems) {
        contentType.fields[index].message = elField.field_validations[elField.specificItems.index].template_json.customerrormessage['defaultvalidationmessage'];
      }
      else if (elField.pattern) {
        contentType.fields[index].message = elField.field_validations[elField.preventSpecific.index].template_json.customerrormessage['defaultvalidationmessage'];
      }
    })
    this.setState({ contentType: contentType })
  }

  handleSubmit() {

    let actionText = this.state.actionText.toLowerCase();
    if (['duplicate', 'delete'].indexOf(actionText) === -1 && this.handleValidation()) {

      let fields = this.state.contentType.fields;
      let postEntry = {
        "entry_status": "Draft",
        "sync_status": "shallow",
        "fields": []
      };
      let localSid = this.state.localInfo.locale_sid;
      this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
      let entrySid = this.props.match.params.entry_sid;
      if (entrySid !== 'add-new') {
        postEntry.fields = fields.map((field, index) => {
          return {
            "field_sid": field.field_sid,
            "field_value": field.value,
            "field_value_sid": field.field_value_sid,
            "locale_sid": field.is_localization_enabled ? localSid : "5d53aaa67e049c22f0321fe7",
            "linked_to_entry_sid": "",
            "linked_to_asset_sid": ""
          }
        });
        APIService.post('other/RCMS.Data/v1/Entries/' + entrySid, postEntry).then((success) => {
          if (success.status === 200 || success.status === 201) {

            this.setState({ 'entry': { 'isAdd': false, ...success.data.results } });
            this.saveLabel();
            toast.success('Entry updated successfully');
            let entrySid = success.data.results.entry_sid;
            let contentTypeSid = success.data.results.content_type_details.content_type_sid
            this.props.history.push('/' + contentTypeSid + '/content-entries/' + entrySid + '/detail');
          }
        }).catch((error) => {
          let message = error.response.data.results.message[Object.keys(error.response.data.results.message)[0]];
          toast.error(message[0])
        }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })
      }
      else {
        postEntry.fields = fields.map((field, index) => {
          return {
            "field_sid": field.field_sid,
            "field_value": field.value,
            "locale_sid": field.is_localization_enabled ? localSid : "5d53aaa67e049c22f0321fe7",
            "linked_to_entry_sid": "",
            "linked_to_asset_sid": ""
          }
        });
        APIService.post('other/RCMS.Data/v1/Entries', postEntry).then((success) => {
          if (success.status === 200 || success.status === 201) {

            this.setState({ 'entry': { 'isAdd': true, ...success.data.results } });
            this.saveLabel();
            toast.success('Entry created successfully');
            let entrySid = success.data.results.entry_sid;
            let contentTypeSid = success.data.results.content_type_details.content_type_sid
            this.props.history.push('/' + contentTypeSid + '/content-entries/' + entrySid + '/detail');
          }
        }).catch((error) => {
          let message = error.response.data.results.message[Object.keys(error.response.data.results.message)[0]];
          toast.error(message[0])
        }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })
      }
    }
    else if (actionText === 'delete') {
      this.toggleDeleteConfirmModal();
    }
  }

  handleValidation() {
    let contentType = this.state.contentType;
    let flag = true;
    contentType.fields.forEach((elField, index) => {
      if (elField.required && (!elField.value || elField.value === '')) {
        contentType.fields[index].message = elField.field_validations[elField.required.index].template_json.customerrormessage['defaultvalidationmessage'];
        flag = false;
      }
      else if (elField.range && elField.value.length < elField.range.min) {
        contentType.fields[index].message = elField.field_validations[elField.range.index].template_json.customerrormessage['defaultvalidationmessage'];
        flag = false;
      }
      else if (elField.pattern) {
        contentType.fields[index].message = elField.field_validations[elField.pattern.index].template_json.customerrormessage['defaultvalidationmessage'];
        flag = false;
      }
      else if (elField.specificItems) {
        contentType.fields[index].message = elField.field_validations[elField.specificItems.index].template_json.customerrormessage['defaultvalidationmessage'];
        flag = false;
      }
      else if (elField.pattern) {
        contentType.fields[index].message = elField.field_validations[elField.preventSpecific.index].template_json.customerrormessage['defaultvalidationmessage'];
        flag = false;
      }
    })

    this.setState({ contentType: contentType })
    return flag;
  }

  saveLabel() {
    let selectedLabels = this.state.selectedLabels
    let entry = this.state.entry;
    this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
    if (selectedLabels.length > 0 && JSON.stringify(entry) !== '{}') {
      let postLabel = [];
      selectedLabels.forEach((elLabel, index) => {
        let label = {
          "label_sid": elLabel.value,
          "assigned_to_entity_id": entry.entry_sid,
          "assigned_to_entity_type": 2
        }
        postLabel.push(APIService.post('other/RCMS.Data/v1/LabelAssignments', label));
      });

      APIService.all(postLabel).then(APIService.spread((success) => {
        if (success.status === 201) {
          toast.success('Label assignment successfully');
        }
      })).catch(error => {
        console.log(error);
        /* let message = error.response.data.results.Message[Object.keys(error.response.data.results.Message)[0]];
        toast.error(message[0]) */
      }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); });
    }
  }

  deleteLabel(url, updatedState) {
    APIService.delete(url).then((success) => {
      if (success.status === 204) {
        this.setState(updatedState)
      }
    }).catch(error => {
      console.log(error);
      /*    let message = error.response.data.results.Message[Object.keys(error.response.data.results.Message)[0]];
         toast.error(message[0]) */
    });
  }

  handleSelection(selectedOptions) {
    let entry = this.state.entry;
    selectedOptions = selectedOptions === null ? [] : selectedOptions;
    if (JSON.stringify(entry) !== '{}' && selectedOptions.length > this.state.selectedLabelSid.length) {
      let itemForAdd = selectedOptions.filter(so => this.state.selectedLabelSid.indexOf(so.value) === -1)

      let postLabel = {
        "label_sid": itemForAdd[0].value,
        "assigned_to_entity_id": this.state.entry.entry_sid,
        "assigned_to_entity_type": 2
      }
      APIService.post('other/RCMS.Data/v1/LabelAssignments', postLabel).then((success) => {
        if (success.status === 201) {
          let selectedLabelSid = this.state.selectedLabelSid
          selectedLabelSid.push(itemForAdd[0].value)
          console.log(selectedLabelSid)
          this.setState({
            selectedLabels: selectedOptions,
            selectedLabelSid: selectedLabelSid
          });
        }
      }).catch(error => {
        console.log(error);
        /* let message = error.response.data.results.Message[Object.keys(error.response.data.results.Message)[0]];
        toast.error(message[0]) */
      });
    }
    else if (JSON.stringify(entry) !== '{}' && selectedOptions.length < this.state.selectedLabelSid.length) {
      let selectedLabelSid = this.state.selectedLabelSid;
      let index;
      selectedOptions.map(so => {
        index = selectedLabelSid.indexOf(so.value)
        if (index !== -1) {
          selectedLabelSid.splice(index, 1);
        }
      });

      selectedLabelSid = selectedLabelSid[0];

      let labelParams = [{ "key": "assigned_to_entity_id", "value": this.state.entry.entry_sid, "condition": "IN" }];

      APIService.get('other/RCMS.Data/v1/Accounts/' + localStorage.getItem('account_sid') + '/LabelAssignments?Page=1&PageSize=10000&Filters=' + JSON.stringify(labelParams)).then((success) => {
        if (success.status === 200) {

          let result = success.data.results.results;

          let assignedLabel = result.filter(la => la.label_sid === selectedLabelSid)

          let deletedLabelUrl = 'other/RCMS.Data/v1/LabelAssignments/' + assignedLabel[0].label_assignment_sid

          let updatedLabelSid = this.state.selectedLabelSid;
          updatedLabelSid.splice(index, 1)

          this.deleteLabel(deletedLabelUrl, {
            selectedLabels: selectedOptions,
            selectedLabelSid: updatedLabelSid
          })

        }
      }).catch(error => {
        console.log(error);
      });

    }
    else {
      this.setState({ selectedLabels: selectedOptions });
    }
  }
  /* shouldComponentUpdate(nextProps, nextState) {
    if (this.state.contentType !== nextState.contentType) {
      console.log('1')
      return true;
    }
    else {
      console.log('2')
      return false;
    }
  } */

  render() {
    let contentType = this.state.contentType;
    let fields = contentType && contentType.fields ? contentType.fields.map((field, index) => {
      field.field_settings.forEach((elFieldSetting, index) => {
        field.field_settings[index].exec_json = JSON.parse(elFieldSetting.execution_json)
      });
      if (field.field_validations) {
        field.field_validations.forEach((elFieldValidation, index) => {
          let valiadation = JSON.parse(elFieldValidation.rule_execution_json);
          field.field_validations[index].template_json = valiadation;
          let title = valiadation.title.toLowerCase();
          field.uicontroltype = 'text';
          switch (title) {
            case 'required field':
              field.required = { 'isRequired': true, index: index };
              break;
            case 'limit character count':
              //field.uicontroltype = 'number';
              field.range = { 'isRange': true, 'index': index, 'type': valiadation.validationChoiceLength.value };
              valiadation.validationChoiceLength.controltypes.forEach((cltrType, index) => {
                if (cltrType.title === 'min' && cltrType.configuredValues !== '') {
                  field.range = { 'min': cltrType.configuredValues, ...field.range }
                }
                if (cltrType.title === 'max' && cltrType.configuredValues !== '') {
                  field.range = { 'max': cltrType.configuredValues, ...field.range }
                }
              })
              break;
            case 'match a specific pattern':
              field.pattern = { 'isPattern': true, 'index': index, type: valiadation.validationChoicePattern.value }
              valiadation.validationChoicePattern.controltypes.forEach((cltrType, index) => {
                if (cltrType.title === 'customexpression' && cltrType.configuredValues !== '') {
                  field.pattern = { 'regex': cltrType.configuredValues, ...field.pattern }
                }
                if (cltrType.title === 'flags' && cltrType.configuredValues !== '') {
                  field.pattern['regex'] = field.pattern.regex + cltrType.configuredValues
                }
              })
              break;
            case 'accept only specified values':

              field.specificItems = { 'isSpecificItems': true, 'index': index }
              valiadation.controltypes.forEach((cltrType, index) => {
                if (cltrType.title === 'predefinedlist' && cltrType.configuredValues !== '') {
                  field.specificItems = { 'predefinedlist': cltrType.configuredValues, ...field.specificItems }
                }
                /* if (cltrType.title === 'flags' && cltrType.configuredValues !== '') {
                  field.pattern['regex'] = field.pattern.regex + cltrType.configuredValues
                } 
                
                */
              })
              break;
            case 'prohibit a specific pattern':
              field.preventSpecific = { 'isSpecificItems': true, 'index': index }
              valiadation.controltypes.forEach((cltrType, index) => {
                if (cltrType.title === 'prohibitexpression' && cltrType.configuredValues !== '') {
                  field.preventSpecific = { 'regex': cltrType.configuredValues, ...field.preventSpecific }
                }
                if (cltrType.title === 'flags' && cltrType.configuredValues !== '') {
                  field.preventSpecific['regex'] = field.preventSpecific.regex + cltrType.configuredValues
                }
              })
              break
            default:
              break;
          }
        });
      }

      return (
        <ContentField key={index} fieldConfiguation={{ 'index': index, ...field }} handleFormInput={this.handleFormInput} />
      )
    }) : [];

    let labels = JSON.stringify(this.state.labels);
    labels = labels.split('name').join('label').split('label_sid').join('value');
    labels = JSON.parse(labels);

    let selectedLabels = JSON.stringify(this.state.selectedLabels);
    selectedLabels = selectedLabels.split('name').join('label').split('label_sid').join('value');
    selectedLabels = JSON.parse(selectedLabels);

    return (
      <React.Fragment>
        <Row className="mt-1">
          <Col md={9}>
            <h2>Content {contentType.name} value</h2>
          </Col>
          <Col md={3}>
            <FormGroup className="mr-1">
              <ButtonDropdown isOpen={this.state.isDropdown} toggle={this.toggleDropdown} className="brl-0">
                <DropdownToggle caret>
                  {this.state.actionText === '' || !this.state.actionText ? 'Action' : this.state.actionText}
                </DropdownToggle>
                <DropdownMenu>
                  {/* <DropdownItem value="Duplicate">Duplicate</DropdownItem> */}
                  <DropdownItem value="Delete">Delete</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <Button color="primary" className="blr-0" onClick={this.handleSubmit}>Save</Button>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={3} className="border">Sidebar</Col>
          <Col md={6}>
            {fields}
            <FormGroup>
              <Select
                value={selectedLabels}
                valueKey="value"
                onChange={this.handleSelection}
                options={labels}
                className="label-select"
                placeholder="Select Label"
                isMulti={true}
                isSearchable={true}
              />
            </FormGroup>
          </Col>
          <Col className="border">
            <Label><b>Status :</b></Label>
            {JSON.stringify(this.state.entry) === '{}' ? 'Draft' : this.state.entry.entry_status}
            <FormGroup className="status-area">
              <Button color="success">{JSON.stringify(this.state.entry) === '{}' ? 'Draft' : this.state.entry.entry_status}
              </Button>
              <ButtonDropdown isOpen={this.state.isStatusDropdown} toggle={this.toggleStatusDropdown} className="blr-0">
                <DropdownToggle caret>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem value="Publish">Publish</DropdownItem>
                  <DropdownItem value="Archive">Archive</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </FormGroup>
          </Col>
        </Row>

        <Modal isOpen={this.state.isDeleteConfirm} toggle={this.toggleDeleteConfirmModal} keyboard={false} centered={true}>
          <ModalHeader toggle={this.toggleDeleteConfirmModal}>
            Are you sure?</ModalHeader>
          <React.Fragment>
            <ModalBody>
              There are no other entries that link to this entry.
            </ModalBody>

            <ModalFooter>
              <Button color="danger" onClick={this.deleteEntry}>
                Yes,delete entry</Button>

              <Button color="secondary" className="ml-2" type="reset" onClick={this.toggleDeleteConfirmModal}>
                Close</Button>
            </ModalFooter>
          </React.Fragment>
        </Modal>
      </React.Fragment>
    );
  }
}

export default connect((state) => {
  return { isLoader: state.isLoader }
})(ContentEntry);

import ContentType from './ContentType/ContentType';
import { Login, Page404, Page500, Register } from './Pages';

export {
  Page404,
  Page500,
  Register,
  Login,
  ContentType
};


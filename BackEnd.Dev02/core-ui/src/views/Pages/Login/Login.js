import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import { Button, Label, Card, CardBody, CardGroup, Col, Container, Input, FormGroup, Row } from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify';
import { FormValidation } from "calidation";
import axios from 'axios';

class Login extends Component {

  _isMounted = false;
  componentDidMount() {
    this._isMounted = true;
  }
  componentDidUnount() {
    this._isMounted = false;
  }
  constructor(props) {
    super(props);
    this.state = {
      user: {
        grant_type: "password",
        emailaddress: "",
        password: ""
      },
      isSubmit: false
    };
    this.handleSignin = this.handleSignin.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSignin(event) {
    let user = this.state.user;

    if (event.target.name === 'emailaddress') {
      user.emailaddress = event.target.value;
      this.setState({ user: user });
    }
    else {
      user.password = event.target.value;
      this.setState({
        user: user
      })
    }
  }

  handleSubmit({ fields, errors, isValid }) {

    this.setState(prevState => ({
      isSubmit: !prevState.isSubmit
    }));
    if (isValid) {
      axios.post(process.env.REACT_APP_API_END_POINT + 'user/Auth.Data/v1/Accounts/Auth', this.state.user, {
        headers: {
          'content-type': 'application/json'
        }
      }).then((success) => {
        let successResult = success.data;

        localStorage.setItem('access_token', successResult.result.access_Token);
        localStorage.setItem('user_sid', successResult.result.user_sid);
        localStorage.setItem('account_sid', successResult.result.account_sid);
        localStorage.setItem('refresh_Token', successResult.result.refresh_Token);
        localStorage.setItem('first_name', successResult.result.first_name);
        localStorage.setItem('last_name', successResult.result.last_name);
        localStorage.setItem('email_address', successResult.result.email_address);
        this.props.history.push('/content-type');

      }).catch((error) => {
        //console.log(error)
        let errorRespnse = error.response.data;
        toast.error(errorRespnse.result.message);
      }).finally(() => {
        this.setState(prevState => ({
          isSubmit: false
        }));
      });
    }
  }
  render() {
  
    let user = this.state.user;
    const config = {
      emailaddress: {
        isRequired: "Email is required!",
        isEmail: "Valid emails only, please!"
      },
      password: {
        isRequired: "Password field required!",
        isMinLength: {
          message: "8 Character at least for password",
          length: 8
        }
      }
    };

    if (localStorage.getItem('access_token')) {
      return <Redirect to='/content-type' />;
    }
    return (
      <div className="app flex-row align-items-center">
        <ToastContainer>
        </ToastContainer>

        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <FormValidation onSubmit={this.handleSubmit} method="post" noValidate config={config}>
                      {({ fields, errors, submitted }) => (
                        <React.Fragment>
                          <h1>Login</h1>
                          <p className="text-muted">Sign In to your account</p>
                          <FormGroup className="mb-3">
                            {/*  <FormGroupAddon addonType="prepend">
                              <FormGroupText>
                                <i className="icon-user"></i>
                              </FormGroupText>
                            </FormGroupAddon> */}
                            <Label for="emailaddress"> Email <span className="invalid-feedback">*</span></Label>
                            <Input type="email" placeholder="Enter email" id="emailaddress" name="emailaddress" value={user.emailaddress} onChange={this.handleSignin} ref="emailaddress" className={submitted && errors.emailaddress ? 'is-invalid' : ''} />

                            {submitted && errors.emailaddress &&
                              <div className="invalid-feedback">{errors.emailaddress}</div>
                            }
                          </FormGroup>
                          <FormGroup className="mb-4">
                            {/* <FormGroupAddon addonType="prepend">
                              <FormGroupText>
                                <i className="icon-lock"></i>
                              </FormGroupText>
                            </FormGroupAddon> */}
                            <Label for="password">Password <span className="invalid-feedback">*</span></Label>
                            <Input type="password" placeholder="Enter password" name="password" value={user.password} onChange={this.handleSignin} id="password" ref="password"
                              className={submitted && errors.password ? 'is-invalid' : ''} />
                            {submitted && errors.password &&
                              <div className="invalid-feedback">{errors.password}</div>
                            }
                          </FormGroup>
                          <Row>
                            <Col xs="6">
                              <Button color="primary" type="submit" disabled={this.state.isSubmit ? true : false}>
                                {this.state.isSubmit ? <i className="fa fa-spinner fa-spin mr-2"></i> : ''}
                                Login</Button>
                            </Col>
                            <Col xs="6" className="text-right">
                              <Button color="link" className="px-0">Forgot password?</Button>
                            </Col>
                          </Row>
                        </React.Fragment>
                      )}
                    </FormValidation>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;

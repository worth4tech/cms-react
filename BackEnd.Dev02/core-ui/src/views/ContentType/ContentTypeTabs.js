import React from 'react';
import {
  TabContent, TabPane, Nav, NavItem, NavLink,
  Row, Col, ListGroupItem, Button, Input, Form, FormGroup, Label,
  Modal, ModalHeader, ModalBody, ModalFooter,

} from 'reactstrap';
import classnames from 'classnames';
import { connect } from 'react-redux';
import ReactJson from 'react-json-view';
/* import { DndProvider } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import update from 'immutability-helper'
 */
import APIService from '../../services/APIService';
import FieldCard from '../../components/FieldCard';
import FieldValidationTab from '../../components/FieldValidationTab';
import FieldSettingTab from '../../components/FieldSettingTab';


/* const ContainerFields = (fields) => {

  const [cards, setCards] = useState(fields);
  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = cards[dragIndex]
      setCards(
        update(cards, {
          $splice: [[dragIndex, 1], [hoverIndex, 0, dragCard]],
        }),
      )
    },
    [cards],
  )
  const renderCard = (card, index) => {
    return (
      <FieldCard
        key={index}
        index={index}
        field={card}
        moveCard={moveCard}
      />
    )
  } 

return (
  <React.Fragment>
    {cards['fields'].map((card, i) => renderCard(card, i))}
  </React.Fragment>
)
}*/


class ContentTypeTabs extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      activeFieldTab: '1',
      allFields: [],
      field: {},
      fieldName: '',
      isAdd: true,
      isDropdown: false,
      isModal: false,
      isSubmit: false,
      isStatusModal: false,
      error: {}
    };

    this.settingTabRef = React.createRef();
    this.validationTabRef = React.createRef();

    this.toggleTabs = this.toggleTabs.bind(this);
    this.getRowFields = this.getRowFields.bind(this);
    this.confirmCreateOrUpdateField = this.confirmCreateOrUpdateField.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleFieldTabs = this.toggleFieldTabs.bind(this);
    this.toggleStatusModal = this.toggleStatusModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
    this.toggleDropdown = this.toggleDropdown.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getRowFields();
  }

  toggleTabs(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  toggleFieldTabs(tab) {
    if (this.state.activeFieldTab !== tab) {
      this.setState({
        activeFieldTab: tab
      });
    }
  }

  toggleModal() {
    this.setState(prevState => ({ isModal: !prevState.isModal }));
  }

  toggleStatusModal() {
    this.setState(prevState => ({ isStatusModal: !prevState.isStatusModal }));
  }

  handleChangeStatus(event, operation, field) {
    event.preventDefault()
    switch (operation) {
      case 'Delete':
        field.is_delete = true;
        break;
      case 'Undelete':
        field.is_delete = false;
        break;
      case 'DisableResponse':
        field.is_include_in_response_enabled = true;
        break;
      case 'EnableResponse':
        field.is_include_in_response_enabled = false;
        break;
      case 'DisableEditing':
        field.is_editing_enabled = true;
        break;
      case 'EnableEditing':
        field.is_editing_enabled = false;
        break;
      default:
        break;
    }
    this.props.handleFieldChange('update', field);
  }
  toggleDropdown() {
    this.setState(prevState => ({
      isDropdown: !prevState.isDropdown
    }));
  }

  getRowFields() {

    this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
    let url = "other/RCMS.Data/v1/FieldTypes?Page=1&PageSize=20"
    APIService.get(url).then((success) => {
      if (success.status === 200) {
        this.setState({
          allFields: success.data.results.results
        });
      }
    }).catch((error) => {
      console.log(error);
    }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); });
  }

  confirmCreateOrUpdateField(isAdd, editableField) {
    if (isAdd) {
      this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
      APIService.get('other/RCMS.Data/v1/FieldTypes/' + editableField.fieldtype_sid).then((success) => {
        this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false });
        this.toggleModal();
        let responseField = { 'field': success.data.results, 'activeFieldTab': '1', fieldName: success.data.results.name };

        responseField.field['field_setting'] = [];
        responseField.field['field_validation'] = [];
        
        this.setState(responseField);

      }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })
    }
    else {
      editableField.fieldtype_settings_detail = [...editableField.field_setting];
      editableField.fieldtype_validations_detail = [...editableField.field_validation];
      let field = {};
      this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
      APIService.get('other/RCMS.Data/v1/FieldTypes/' + editableField.field_type_sid).then((success) => {
        this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false });
        /* Merge validation of existing field and remaning from main object */
        field = success.data.results;

        field.validationIds = field.fieldtype_validations_detail.map((fv) => fv.fieldtype_validation_sid)

        field.settingIds = field.fieldtype_settings_detail.map((fs) => fs.fieldtype_setting_sid);

        field.fieldtype_settings_detail.forEach((fSetting, index) => {

          let settingId = editableField.fieldtype_settings_detail[index] !== undefined ? editableField.fieldtype_settings_detail[index].fieldtype_setting_sid : '';

          if (field.settingIds.indexOf(settingId) === -1) {
            editableField.fieldtype_settings_detail.push(fSetting);
          }
        });

        field.fieldtype_validations_detail.forEach((fValidate, index) => {
          let validationId = editableField.fieldtype_validations_detail[index] !== undefined ? editableField.fieldtype_validations_detail[index].fieldtype_validation_sid : '';
          if (field.validationIds.indexOf(validationId) === -1) {
            editableField.fieldtype_validations_detail.push(fValidate);
          }
        });

        this.toggleModal();
        this.setState({ field: editableField, isAdd: false, 'fieldName': editableField.name, isSubmit: false, activeFieldTab: '1' })

      }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })
    }

  }

  handleInput(event) {
    let field = this.state.field;
    field[event.target.name] = event.target.value;
    this.setState({ 'field': field })
  }

  shouldComponentUpdate(nextProps, nextState) {
    if ((this.state.isModal !== nextState.isModal)
      || (this.state.field.name !== undefined && this.state.field.name === nextState.fieldName)
      || (this.state.activeFieldTab !== nextState.activeFieldTab)
      || (this.state.activeTab !== nextState.activeTab)
      || (this.state.isDropdown !== nextState.isDropdown)
      || (this.state.allFields !== nextState.allFields)
      || (this.state.isSubmit !== nextState.isSubmit)
      || (this.state.field !== nextState.field)) {
      return true;
    }
    else {
      return false;
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ isSubmit: true });
    let field = this.state.field;
    if (field.name === '') {
      let error = { 'name': 'Name is required' }
      this.setState(prevState => ({
        error: { prevState, ...error }
      }));
    }
    else {
      let postField = {
        ...field,
        'is_editing_enabled': true,
        'is_include_in_response_enabled': true,
        'is_delete': true,
        'field_validation': [],
        'field_setting': []
      }
      if (this.validationTabRef.current.state.fieldValidations) {
        this.validationTabRef.current.state.fieldValidations.forEach(function (fv) {
          if (fv.checked === true) {
            fv.rule_execution_json = fv.rule_template_json
            fv.field_type_validation_sid = fv.fieldtype_validation_sid
            postField.field_validation.push(fv);
          }
        });
      }

      if (this.settingTabRef.current.state.fieldSettings) {
        this.settingTabRef.current.state.fieldSettings.forEach(function (fs) {
          if (fs.checked === true) {
            fs.execution_json = JSON.stringify(fs.execution_json);
            postField.field_setting.push(fs);
          }
        });
      }
      if (this.state.isAdd) {
        this.props.handleFieldChange('add', postField);
      }
      else {
        this.props.handleFieldChange('update', postField);
      }
      this.setState({ field: {} });
      this.toggleModal();
    }
  }

  componentWillUnmount() { this._isMounted = false; }

  render() {

    let contentType = localStorage.getItem('content_type') ? JSON.parse(localStorage.getItem('content_type')) : this.props.contentType;

    let contentTypeFields = contentType.fields && contentType.content_type_sid !== 'add-new'
      ?
      contentType.fields.map((contentTypeField, index) => {
        return (
          <FieldCard
            field={contentTypeField}
            index={index}
            key={index}
            confirmCreateOrUpdateField={this.confirmCreateOrUpdateField}
            handleChangeStatus={this.handleChangeStatus}
          />
        );
      }) : '';

    let allFields = this.state.allFields.map((rowField, index) =>
      <ListGroupItem key={rowField.fieldtype_sid} onClick={() => { this.confirmCreateOrUpdateField(true, rowField) }}
        className={index === 0 ? 'cursor-ptr mb-1 rounded' : 'cursor-ptr my-1 rounded'}
      >{rowField.name}</ListGroupItem>
    );

    let field = this.state.field;

    return (
      <React.Fragment>
        <div className="container-fluid content-type-container">
          <Row>
            <Col md={8}>
              <Nav tabs className="content-tab-nav">
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === '1' })}
                    onClick={() => { this.toggleTabs('1'); }}
                  >
                    Fields({contentType && contentType.fields && this.props.contentType.content_type_sid !== 'add-new' ? contentType.fields.length : 0})
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === '2' })}
                    onClick={() => { this.toggleTabs('2'); }}
                  >
                    JSON Previewer
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeTab} className="content-tab">
                <TabPane tabId="1">
                  {contentTypeFields}
                  {/* <DndProvider backend={HTML5Backend}>
                    {contentTypeFields}
                  </DndProvider> */}
                </TabPane>
                <TabPane tabId="2">
                  <ReactJson src={contentType.fields} />
                </TabPane>
              </TabContent>
            </Col>
            <Col md={4} className="top-54">
              {allFields}
            </Col>
          </Row>
        </div>

        <Modal isOpen={this.state.isModal} toggle={this.toggleModal} size="lg" keyboard={false} centered>
          <Form onSubmit={this.handleSubmit} method="post">
            <ModalHeader toggle={this.toggleModal}>
              {this.state.isAdd ? 'Add field' : 'Edit field'}
            </ModalHeader>
            <ModalBody>
              <Row>
                <Col md={12}>
                  <FormGroup>
                    <Label for="name">Name <span className="text-danger">*</span></Label>
                    <Input type="text" name="name" id="name" defaultValue={field.name || ''}
                      onChange={this.handleInput}
                      className={this.isSubmit && field.name === '' ? 'is-invalid' : ''} />
                    {this.isSubmit && field.name === '' ?
                      <div className="invalid-feedback">{this.state.error.name}</div>
                      : ''}
                  </FormGroup>
                </Col>
              </Row>
              <Nav tabs className="content-tab-nav">
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFieldTab === '1' })}
                    onClick={() => { this.toggleFieldTabs('1'); }}
                  >
                    Settings
                </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFieldTab === '2' })}
                    onClick={() => { this.toggleFieldTabs('2'); }}
                  >
                    Validations
                  </NavLink>
                </NavItem>
              </Nav>

              <TabContent activeTab={this.state.activeFieldTab} className="content-tab">
                <TabPane tabId="1">
                  <Row>
                    <Col md={12}>
                      {
                        field.fieldtype_settings_detail !== undefined ?
                          <FieldSettingTab fields={field.fieldtype_settings_detail}
                            ref={this.settingTabRef} />
                          : ''
                      }
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="2">
                  <Row>
                    <Col md={12}>
                      {
                        field.fieldtype_validations_detail !== undefined ?
                          <FieldValidationTab fields={field.fieldtype_validations_detail}
                            //handleFormInput={this.handleFormInput}
                            ref={this.validationTabRef} />
                          : ''
                      }
                    </Col>
                  </Row>
                </TabPane>
              </TabContent>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type="submit">Submit</Button>
              <Button color="danger" className="ml-2" type="reset" onClick={this.toggleModal}>
                Close</Button>
            </ModalFooter>
          </Form>
        </Modal>

        <Modal isOpen={this.state.isStatusModal} toggle={this.toggleStatusModal} size="md" keyboard={false} centered>
          <ModalHeader toggle={this.toggleStatusModal}>
            Add new field</ModalHeader>
          <ModalBody>
            <p>Confirmation Modal</p>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" type="submit">Submit</Button>
            <Button color="danger" className="ml-2" type="reset" onClick={this.toggleStatusModal}>
              Close</Button>
          </ModalFooter>
        </Modal>
      </React.Fragment >
    );
  }
}
export default connect((state) => {
  return { isLoader: state.isLoader }
})(ContentTypeTabs);
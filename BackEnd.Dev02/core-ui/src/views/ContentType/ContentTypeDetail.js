import React, { Component } from 'react';
import { Label, Row, Col, Button, Input, FormGroup, Modal, ModalHeader, ModalBody, ModalFooter, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import { FormValidation } from "calidation";
import ContentTypeTabs from './ContentTypeTabs';
import APIService from '../../services/APIService';

class ContentTypeDetail extends Component {
  _isMounted = false;
  isSubmit = false;
  constructor(props) {
    super(props);
    let account_sid = localStorage.getItem('account_sid');
    this.state = {
      contentType: {
        content_type_library_sid: "",
        name: "",
        account_sid: account_sid,
        description: "",
        fields: [],
        content_type_sid: this.props.match.params.contenttype_sid
      },
      isModal: false,
      actionText: 'Action',
      increment: 1
    };
    this.handleProps = this.handleProps.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.resetContentType = this.resetContentType.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    let content_type_sid = this.state.contentType.content_type_sid;

    if (content_type_sid === 'add-new') {
      localStorage.removeItem('content_type');
      this.toggleModal()
    }
    else if (content_type_sid === 'is-temp') {
      let contentType = { contentType: JSON.parse(localStorage.getItem('content_type')) };
      this.setState(contentType)
    }
    else {
      let url = 'other/RCMS.Data/v1/ContentTypes/' + content_type_sid;
      this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
      APIService.get(url).then((success) => {
        let result = success.data.results;
        let existContentType = localStorage.getItem('content_type');

        localStorage.removeItem('content_type');

        if (existContentType) {

          existContentType = JSON.parse(existContentType)

          this.setState({
            contentType: { ...result, ...existContentType }
          })
          localStorage.setItem('content_type', JSON.stringify({ ...result, ...existContentType }));
        }
        else {
          this.setState({
            contentType: result
          })
          localStorage.setItem('content_type', JSON.stringify(result));
        }
      }).catch((error) => {

      }).finally(() => this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }))

    }
  }
  componentWillUnmount() { this._isMounted = false; }

  handleProps(newCount) {
    this.setState({ increment: newCount });
  }

  handleInput(event) {
    let contentType = this.state.contentType;
    if (event.target.name === 'name') {
      contentType.name = event.target.value;
      this.setState({ contentType: contentType });
    }
    else {
      contentType.description = event.target.value;
      this.setState({
        contentType: contentType
      })
    }
  }

  toggleModal() {
    this.setState(prevState => ({
      isModal: !prevState.isModal
    }));
  }
  toggleDropdown(event) {
    let actionText = event.target !== null ? event.target.value : 'Action';
    this.setState(prevState => ({
      actionText: actionText
    }));
  }
  handleSubmit({ fields, errors, isValid }) {
    let contentType = this.state.contentType;
    if (contentType.name !== '' && contentType.description !== '') {
      let contentType = this.state.contentType;
      this.toggleModal();

      contentType.fields = contentType.content_type_sid === undefined ? [] : contentType.fields;
      contentType.content_type_sid = 'is-temp';
      localStorage.setItem('content_type', JSON.stringify(contentType))
    }
  }

  resetContentType() {
    let contentType = JSON.parse(localStorage.getItem('content_type'));
    if (contentType) {
      this.props.history.push('/content-type/' + contentType.content_type_sid + '/detail')
    }
    else {
      this.props.history.push('/content-type')
    }
  }

  handleSave() {
    let actionText = this.state.actionText;
    if (actionText === 'Delete') {
      this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });

      let url = "other/RCMS.Data/v1/ContentTypes/" + this.state.contentType.content_type_sid

      APIService.delete(url).then((success) => {
        if (success.status === 204) {
          this.props.history.push('/content-type');
        }
      }).catch((error) => {

        let message = error.response.data.results.Message[Object.keys(error.response.data.results.Message)[0]];
        toast.error(message[0]);
      }).finally(() => { this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false }); })
    }
    else if (actionText === 'Duplicate') {
      console.log('Duplidate')
    }
    else {
      let contentType = JSON.parse(localStorage.getItem('content_type'));
      let url = 'other/RCMS.Data/v1/ContentTypes/';
      if (this.props.match.params['contenttype_sid'] === 'is-temp' || contentType.content_type_sid === 'is-temp') {
        delete contentType.content_type_sid;
      }
      else {
        contentType.content_type_sid = this.props.match.params['contenttype_sid']
        url = url + this.props.match.params['contenttype_sid'];
      }

      this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
      APIService.post(url, contentType).then((success) => {
        if (success) {
          let contentType = success.data.results;
          localStorage.setItem('content_type', JSON.stringify(contentType));
          this.setState({ contentType: contentType })
          this.props.history.push('/content-type/' + contentType.content_type_sid + '/detail')
          toast.success('Content type created successfully');
        }

      }).catch((error) => {
        //let message = error.response.data;
        toast.error('Oops! Something went wrong ');
      }).finally(() => {
        this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false });
      });
    }
  }

  handleFieldChange(operation, payload = {}) {
    let contentType = this.state.contentType;

    delete payload['fieldtype_settings_detail'];
    delete payload['fieldtype_validations_detail'];
    payload['field_type_sid'] = payload['fieldtype_sid'];
    payload['sequence'] = contentType.fields.length + 1;
    if (operation === 'add') {
      contentType.fields.push(payload);
    }
    else {
      contentType.fields[payload.index] = payload;
    }
    localStorage.setItem('content_type', JSON.stringify(contentType));
    this.setState({ contentType: contentType });
  }

  render() {
    let contentType = this.state.contentType;
    //let contentType = JSON.parse(localStorage.getItem('content_type'));
    let content_type_sid = this.props.match.params.contenttype_sid;
    let config = {
      name: {
        isRequired: "Name is required!"
      },
      description: {
        isRequired: "Description is required!"
      }
    };
    return (
      <div className="app flex-row align-items-center">
        <Row className="fixed-top sec-head">
          <Col md={3}>
            <b>{content_type_sid !== 'add-new' && contentType ?
              contentType.name :
              'Untitled'}</b>
            <Button className="ml-1" color="primary" onClick={this.toggleModal}>Edit</Button>
          </Col>
          <Col md={6}>
            <p className="p-color">
              {
                contentType && contentType.description !== '' ?
                  contentType.description :
                  ''}
            </p>
          </Col>
          <Col md={3}>
            <ButtonDropdown isOpen={this.state.isDropdown} toggle={this.toggleDropdown} className="mr-2" value="Action">
              <DropdownToggle caret>
                {this.state.actionText === '' || !this.state.actionText ? 'Action' : this.state.actionText}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem value="Duplicate">Duplicate</DropdownItem>
                <DropdownItem value="Delete">Delete</DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
            <Button color="success" onClick={this.handleSave}>Save</Button>
          </Col>
        </Row>
        <React.Fragment>
          <ContentTypeTabs contentType={contentType} handleFieldChange={this.handleFieldChange}
            increment={this.state.increment}
            handleProps={this.handleProps} />
        </React.Fragment>

        <Modal isOpen={this.state.isModal} toggle={this.toggleModal} size="lg" keyboard={false}
          onClosed={this.resetContentType}
        >
          <ModalHeader toggle={this.toggleModal}>
            Create new content type</ModalHeader>
          <FormValidation onSubmit={this.handleSubmit} method="post" noValidate config={config}>
            {({ fields, errors, submitted }) => (
              <React.Fragment>
                <ModalBody>
                  <FormGroup className="mb-3">
                    <Label for="name"> Name <span className="invalid-feedback">*</span></Label>
                    <Input type="text" placeholder="Enter content type name" id="name" name="name" value={contentType.name} onChange={this.handleInput} className={submitted && contentType.name === '' ? 'is-invalid' : ''} />

                    {submitted && contentType.name === '' ?
                      <div className="invalid-feedback">{errors.name}</div>
                      : ''}
                  </FormGroup>
                  <FormGroup className="mb-4">
                    <Label for="descriptions">Description <span className="invalid-feedback">*</span></Label>
                    <Input type="textarea" placeholder="Enter description" name="description" value={contentType.description} onChange={this.handleInput} id="description"
                      className={submitted && contentType.description === '' ? 'is-invalid' : ''} />
                    {submitted && contentType.description === '' ?
                      <div className="invalid-feedback">{errors.description}</div>
                      : ''}
                  </FormGroup>
                </ModalBody>

                <ModalFooter>
                  <Button color="primary" type="submit">
                    <i className="fa fa-spinner fa-spin mr-2"></i> : ''}
                    Submit</Button>

                  <Button color="danger" className="ml-2" type="reset" onClick={this.toggleModal}>
                    Close</Button>
                </ModalFooter>
              </React.Fragment>
            )}
          </FormValidation>
        </Modal>
      </div>
    );
  }
}
export default connect((state) => {
  return { isLoader: state.isLoader }
})(ContentTypeDetail);


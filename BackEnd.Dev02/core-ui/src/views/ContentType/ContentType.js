import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Button, Input, FormGroup } from 'reactstrap';
import moment from 'moment';
import { connect } from 'react-redux';
import APIService from '../../services/APIService';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, { PaginationProvider } from 'react-bootstrap-table2-paginator';

const TableWrapper = ({ data, columns, page, sizePerPage, onTableChange, pagination }) => (
  <React.Fragment>
    <PaginationProvider
      pagination={
        paginationFactory({
          custom: true,
          page,
          sizePerPage
        })
      }
    >
      {
        ({
          paginationProps,
          paginationTableProps
        }) => (
            <React.Fragment>
              <BootstrapTable
                remote
                keyField="contenttype_sid"
                data={data}
                options={{ noDataText: "No content type(s) found" }}
                columns={columns}
                onTableChange={onTableChange}
                { ...paginationTableProps }
                defaultSorted={[{
                  dataField: 'name',
                  order: 'desc'
                }]}
                hover responsive bootstrap4
              >
              </BootstrapTable>
              <div role="group" className="float-right mb-1">
                <Button color="link" disabled={pagination.Page === 1 ||
                  data.length < pagination.PageSize ? true : false}
                  onClick={() => { onTableChange('sort', { "sortOrder": pagination.SortOrder, "sortField": pagination.SortColumn, "page": --pagination.Page, "sizePerPage": pagination.PageSize }) }}><i className="fas fa-chevron-circle-left fa-2x"></i></Button>

                <Button color="link" className="ml-1"
                  disabled={data.length < pagination.PageSize ? true : false}
                  onClick={() => { onTableChange('sort', { "sortOrder": pagination.SortOrder, "sortField": pagination.SortColumn, "page": ++pagination.Page, "sizePerPage": pagination.PageSize }) }}><i className="fas fa-chevron-circle-right fa-2x"></i></Button>
              </div>
            </React.Fragment>
          )
      }
    </PaginationProvider>
  </React.Fragment>
);

class ContentType extends Component {

  constructor(props) {
    super(props);
    this.state = {
      "contentTypes": [],
      "paginationAndSort": {
        "Page": 1,
        "PageSize": 10,
        "SortColumn": "name",
        "SortOrder": "desc",
        "SearchText": ""
      },
      "columns": [
        {
          "text": "Name",
          "dataField": "name",
          "sort": true
        },
        {
          "text": "Description",
          "dataField": "description",
          "sort": true
        },
        {
          "text": "Updated At",
          "sort": true,
          "dataField": "last_modified_datetime",
          "formatter": (cell, row, rowIndex, formatExtraData) => {
            return row.last_modified_datetime ? moment(new Date(row.last_modified_datetime)).fromNow() : '';
          }
        },
        {
          "text": "Action(s)",
          "dataField": 'action',
          "isDummyField": true,
          "sort": false,
          formatter: (cell, row, rowIndex, extraData) => (
            <Link className="btn btn-link" to={"/content-type/" + row.contenttype_sid + "/detail"}> Details</Link >
          ),
        }
      ],
      isLoading: false,
    };
    this.handleSearchText = this.handleSearchText.bind(this);
    this.search = this.search.bind(this);
  }

  handleTableChanges = (type, { sortOrder, sortField, page, sizePerPage }) => {
    let searchText = this.state ? this.state.paginationAndSort.SearchText : '';
    let account_sid = localStorage.getItem('account_sid')
    let url = 'other/RCMS.Data/v1/Accounts/' + account_sid + '/ContentTypes?SearchText=' + searchText + '&Page=' + page + '&PageSize=' + sizePerPage + '&SortColumn=' + sortField + '&SortOrder=' + sortOrder + '&Filters&FiltersList';

    this.props.dispatch({ type: 'TOGGLE_LOADER', payload: true });
    APIService.get(url).then((success) => {
      if (success.status === 200) {
        let state = {
          contentTypes: success.data.results.results,
        }
        this.setState(state);
      }
    }).catch(error => {
      console.log(error)
    }).finally(() => {
      this.props.dispatch({ type: 'TOGGLE_LOADER', payload: false });
    });
  }

  handleSearchText(event) {
    let pagination = this.state.paginationAndSort
    pagination.SearchText = event.target.value;
    this.setState({
      paginationAndSort: pagination
    })
  }

  search(event) {
    event.preventDefault();
    let pagination = {
      "sortOrder": this.state.paginationAndSort.SortOrder,
      "sortField": this.state.paginationAndSort.SortColumn,
      "page": this.state.paginationAndSort.Page,
      "sizePerPage": this.state.paginationAndSort.PageSize,
      "SearchText": this.state.paginationAndSort.SearchText
    }
    this.handleTableChanges('sort', pagination);
  }

  render() {
    let data = this.state.contentTypes;
    let columns = this.state.columns;
    let paginationAndSort = this.state.paginationAndSort;

    return (
      <div className="animated fadeIn">
        <Row className="mt-2">
          <Col md="9" >
            <div className="form-inline">
              <FormGroup>
                <Input type="text" placeholder="Search" id="SearchText" name="SearchText" value={paginationAndSort.SearchText} onChange={this.handleSearchText} className="mr-1" />
                <Button color="primary" onClick={this.search}><i className="fa fa-search"></i>Search</Button>
              </FormGroup>
            </div>
          </Col>
          <Col md="3">
            <Button color="success" onClick={() => {
              this.props.history.push('content-type/add-new/detail')
            }}><i className="fa fa-plus"></i> Content Type</Button>
          </Col>
        </Row>
        <Row>
          <Col md="12" className="mt-2">
            <TableWrapper
              data={data}
              page={paginationAndSort.page}
              columns={columns}
              sizePerPage={paginationAndSort.PageSize}
              onTableChange={this.handleTableChanges}
              pagination={paginationAndSort}
              selectRow={this.handleRowSelection}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default connect((state) => {
  return { isLoader: state.isLoader }
})(ContentType);



import axios from 'axios';

let API_URL = process.env.REACT_APP_API_END_POINT;

let headers = {
  'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
  'content-type': 'application/json',
  'ServiceBin_SID': 'SB32B4D86B-9501-43C8-8753-46608AF8C49E',
  'account_SID': localStorage.getItem('account_sid'),
}

let config = {
  baseURL: API_URL,
  headers: headers
}

const APIService = axios.create(config);

APIService.interceptors.request.use((request) => {

  if (request.method === 'post' && request.url === 'other/RCMS.Data/v1/Entries') {
    request.headers['content_type_sid'] = localStorage.getItem('content_type_sid');
  }
  else if (['post', 'delete'].indexOf(request.method) !== -1 && request.url.search('LabelAssignments') !== -1) {
    request.headers['chat_account_sid'] = localStorage.getItem('chat_account_sid') ? localStorage.getItem('chat_account_sid') : 'OA01461c26-5cfd-4afe-a7db-04f4f180ed88';
  }

  return request;
}, (error) => {
  return Promise.reject(error);
});


APIService.interceptors.response.use((response) => {
  return response;
}, (error) => {
  if (error.response.status === 401) {
    let contentType = localStorage.getItem('content_type');
    localStorage.clear();
    localStorage.setItem('content_type', contentType);
    window.location.href = "/"
  }
  return Promise.reject(error);
});

APIService.all = axios.all;
APIService.spread = axios.spread;
export default APIService;
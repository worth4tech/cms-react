import React from 'react';
import FormValidationControl from './FormValidationControl';

class FieldSettingTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fieldSettings: this.props.fields
    };
    this.handleFormInput = this.handleFormInput.bind(this);
  }

  handleFormInput(setting) {

    let allFields = this.state.fieldSettings;
    allFields[setting.index] = setting;
    allFields[setting.index].execution_template_json = JSON.stringify(setting.execution_json);
    allFields[setting.index].field_type_setting_sid = setting.fieldtype_setting_sid;
    this.setState({ fieldSettings: allFields });
  }

  render() {

    let fields = this.state.fieldSettings.map((tempSetting, index) => {
      tempSetting['execution_json'] = JSON.parse(tempSetting.execution_template_json);
      return (
        <React.Fragment key={"frag_" + index}>
          <FormValidationControl config={{ type: 'setting', 'index': index, ...tempSetting }} handleFormInput={this.handleFormInput} />
        </React.Fragment>);
    })
    return (
      <React.Fragment>
        {fields}
      </React.Fragment>
    );
  }
}

export default FieldSettingTab;
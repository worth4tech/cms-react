import React from 'react';
import FormValidationControl from './FormValidationControl';

class FieldValidationTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fieldValidations: this.props.fields
    };
    this.handleFormInput = this.handleFormInput.bind(this);
  }

  handleFormInput(validation) {
    let allFields = this.state.fieldValidations;
    allFields[validation.index] = validation;

    allFields[validation.index].rule_template_json = JSON.stringify(validation.template_json);

    this.setState({ fieldValidations: allFields });
  }

  render() {
    let fields = this.state.fieldValidations.map((tempValidate, index) => {
      tempValidate['template_json'] = JSON.parse(tempValidate.rule_template_json);
      return (
        <React.Fragment key={"frag_" + index} >
          <FormValidationControl config={{ type: 'validation', 'index': index, ...tempValidate }} handleFormInput={(event) => this.handleFormInput(event, { type: 'validation', 'index': index, ...tempValidate })} />
        </React.Fragment>
      );
    })
    return (
      <React.Fragment>
        {fields}
      </React.Fragment>
    );
  }
}

export default FieldValidationTab;
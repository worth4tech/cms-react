import React from 'react';
import {
  Card,
  CardBody, Button,
  ButtonDropdown, DropdownToggle,
  DropdownMenu, DropdownItem
} from 'reactstrap';


class FieldCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isDropdown: false
    }
    this.toggleDropdown = this.toggleDropdown.bind(this);
  }
  toggleDropdown() {
    this.setState(prevState => ({
      isDropdown: !prevState.isDropdown,
    }));
  }
  render() {
    let contentTypeField = this.props.field;
    return (
      <Card>
        <CardBody className=".d-inline-flex field-card">

          <span className="p-2">{contentTypeField.name}</span>

          <span className="float-right">

            <span className="text-secondary p-1">{!contentTypeField.is_editing_enabled ? 'Editing Disabled' : ''}</span>

            <span className="text-secondary p-1">{!contentTypeField.is_delete ? 'Deleted' : ''}</span>


            <span className="text-secondary p-1">{!contentTypeField.is_include_in_response_enabled ? 'Response Disabled' : ''}</span>


            <Button color="link" onClick={(event) => this.props.confirmCreateOrUpdateField(false, { 'index': this.props.index, ...contentTypeField })}>Settings</Button>

            <ButtonDropdown isOpen={this.state.isDropdown} toggle={this.toggleDropdown} className="mr-2" value="Action">

              <DropdownToggle className="btn btn-link status-dropdown">
                ...
              </DropdownToggle>
              <DropdownMenu>
                {
                  contentTypeField.is_editing_enabled === true ?
                    <DropdownItem value="DisableEditing" onClick={(event) => this.props.handleChangeStatus(event, 'DisableEditing', { 'index': this.props.index, ...contentTypeField })}>
                      Disable editing
                      </DropdownItem> :
                    <DropdownItem value="EnableEditing" onClick={(event) => this.props.handleChangeStatus(event, 'EnableEditing', { 'index': this.props.index, ...contentTypeField })}>
                      Enable editing
                        </DropdownItem>
                }

                {
                  contentTypeField.is_include_in_response_enabled === true ?
                    <DropdownItem value="DisableResponse" onClick={(event) => this.props.handleChangeStatus(event, 'DisableResponse', { 'index': this.props.index, ...contentTypeField })}>
                      Disable in response
                        </DropdownItem> :
                    <DropdownItem value="EnableResponse" onClick={(event) => this.props.handleChangeStatus(event, 'EnableResponse', { 'index': this.props.index, ...contentTypeField })}>
                      Enable in response
                    </DropdownItem>
                }

                {
                  contentTypeField.is_delete === false ?
                    <DropdownItem value="Delete" onClick={(event) => this.props.handleChangeStatus(event, 'Delete', { 'index': this.props.index, ...contentTypeField })}>
                      Delete
                        </DropdownItem> :
                    <DropdownItem value="Undelete" onClick={(event) => this.props.handleChangeStatus(event, 'Undelete', { 'index': this.props.index, ...contentTypeField })}>
                      Undelete
                        </DropdownItem>
                }
              </DropdownMenu>
            </ButtonDropdown>
          </span>

        </CardBody>
      </Card>
    );
  }
}
export default FieldCard;




/* import React, { useRef } from 'react'
import { useDrag, useDrop } from 'react-dnd'
import {
  Card,
  CardBody, Button,
  ButtonDropdown, DropdownToggle,
  DropdownMenu, DropdownItem
} from 'reactstrap';


const style = {
  border: '1px dashed gray',
  padding: '0.5rem 1rem',
  marginBottom: '.5rem',
  backgroundColor: 'white',
  cursor: 'move',
}
const FieldCard = ({ index, field, moveCard }) => {
  const ref = useRef(null)
  const [, drop] = useDrop({
    accept: 'Card',
    hover(item, monitor) {
      if (!ref.current) {
        return
      }
      const dragIndex = item.index
      const hoverIndex = index
      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }
      // Determine rectangle on screen
      const hoverBoundingRect = ref.current.getBoundingClientRect()
      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
      // Determine mouse position
      const clientOffset = monitor.getClientOffset()
      // Get pixels to the top
      const hoverClientY = clientOffset.y - hoverBoundingRect.top
      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }
      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }
      // Time to actually perform the action
      moveCard(dragIndex, hoverIndex)
      item.index = hoverIndex
    },
  })
  const [{ isDragging }, drag] = useDrag({
    item: { type: 'Card', index },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  })
  const opacity = isDragging ? 0 : 1
  drag(drop(ref))
  let contentTypeField = field
  return (
    <Card style={{ ...style, opacity }}>
      <CardBody className=".d-inline-flex field-card">

        <span className="p-2">{contentTypeField.name}</span>

        <span className="float-right">

          <span className="text-secondary p-1">{!contentTypeField.is_editing_enabled ? 'Editing Disabled' : ''}</span>

          <span className="text-secondary p-1">{!contentTypeField.is_delete ? 'Deleted' : ''}</span>


          <span className="text-secondary p-1">{!contentTypeField.is_include_in_response_enabled ? 'Response Disabled' : ''}</span>


          <Button color="link" onClick={(event) => this.props.handleSetting(event, { 'index': this.props.index, ...contentTypeField })}>Settings</Button>

          <ButtonDropdown isOpen={this.state.isDropdown} toggle={this.toggleDropdown} className="mr-2" value="Action">

            <DropdownToggle className="btn btn-link status-dropdown">
              ...
              </DropdownToggle>
            <DropdownMenu>
              {
                contentTypeField.is_editing_enabled === true ?
                  <DropdownItem value="DisableEditing" onClick={(event) => this.props.handleChangeStatus(event, 'DisableEditing', { 'index': this.props.index, ...contentTypeField })}>
                    Disable editing
                      </DropdownItem> :
                  <DropdownItem value="EnableEditing" onClick={(event) => this.props.handleChangeStatus(event, 'EnableEditing', { 'index': this.props.index, ...contentTypeField })}>
                    Enable editing
                        </DropdownItem>
              }

              {
                contentTypeField.is_include_in_response_enabled === true ?
                  <DropdownItem value="DisableResponse" onClick={(event) => this.props.handleChangeStatus(event, 'DisableResponse', { 'index': this.props.index, ...contentTypeField })}>
                    Disable in response
                        </DropdownItem> :
                  <DropdownItem value="EnableResponse" onClick={(event) => this.props.handleChangeStatus(event, 'EnableResponse', { 'index': this.props.index, ...contentTypeField })}>
                    Enable in response
                    </DropdownItem>
              }

              {
                contentTypeField.is_delete === false ?
                  <DropdownItem value="Delete" onClick={(event) => this.props.handleChangeStatus(event, 'Delete', { 'index': this.props.index, ...contentTypeField })}>
                    Delete
                        </DropdownItem> :
                  <DropdownItem value="Undelete" onClick={(event) => this.props.handleChangeStatus(event, 'Undelete', { 'index': this.props.index, ...contentTypeField })}>
                    Undelete
                        </DropdownItem>
              }
            </DropdownMenu>
          </ButtonDropdown> 
        </span>

      </CardBody>
    </Card>
  )
}
export default FieldCard */

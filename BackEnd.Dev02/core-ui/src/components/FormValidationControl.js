import React, { Component } from 'react';
import { Input, FormGroup, Label, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';

class FormValidationControl extends Component {

  constructor(props) {
    super(props);
    this.selectRef = React.createRef();
    this.handleInput = this.handleInput.bind(this);
    this.renderFieldValidation = this.renderFieldValidation.bind(this);
  }

  handleInput(event) {

    let config = this.props.config;

    if (event.target.type === 'checkbox' && event.target.className !== 'file-type form-check-input') {
      config.checked = event.target.checked ? true : false;

      if (['limit character count', 'match a specific pattern', 'limit number of properties', 'accept only specified number range'].indexOf(config.title.toLowerCase()) !== -1 && !config.template_json['resetDropdownOptions']) {

        config.template_json['resetDropdownOptions'] = JSON.stringify(config.template_json['dropdownoptions'])
      }
    }
    else if (event.target.type === 'text' && ['sp-pattern form-control', 'predefined-values form-control', 'prohibited-pattern form-control'].indexOf(event.target.className) === -1) {
      config.template_json.customerrormessage['defaultvalidationmessage'] = event.target.value;
    }
    else if (event.target.id === 'validationChoiceLength') {
      config.template_json['dropdownoptions'] = JSON.parse(config.template_json['resetDropdownOptions']);

      config.template_json['validationChoiceLength'] = config.template_json['dropdownoptions'].filter((validation, index) => validation.value === event.target.value);

      config.template_json['validationChoiceLength'] = config.template_json['validationChoiceLength'][0];
    }
    else if (event.target.type === 'number') {

      if ('validationChoiceLength' in config.template_json) {

        let mainIndex = ['between', 'at least', 'not more than'].indexOf(config.template_json['validationChoiceLength'].value.toLowerCase());

        config.template_json['validationChoiceLength'].controltypes[event.target.id].configuredValues = event.target.value;
        config.template_json.dropdownoptions[mainIndex] = config.template_json['validationChoiceLength'];
      }
      else if ('validationJSONLength' in config.template_json) {

        let mainIndex = ['between', 'at least', 'not more than'].indexOf(config.template_json['validationJSONLength'].value.toLowerCase());

        config.template_json['validationJSONLength'].controltypes[event.target.id].configuredValues = event.target.value;
        config.template_json.dropdownoptions[mainIndex] = config.template_json['validationJSONLength'];
      }
      else if ('validationNumberLength' in config.template_json) {

        let mainIndex = ['between', 'greater Or equal than', 'less Or equal than'].indexOf(config.template_json['validationNumberLength'].value.toLowerCase());

        config.template_json['validationNumberLength'].controltypes[event.target.id].configuredValues = event.target.value;
        config.template_json.dropdownoptions[mainIndex] = config.template_json['validationNumberLength'];
      }

    }
    else if (event.target.id === 'validationChoicePattern') {
      config.template_json['dropdownoptions'] = JSON.parse(config.template_json['resetDropdownOptions']);

      config.template_json['validationChoicePattern'] = config.template_json['dropdownoptions'].filter((validation, index) => validation.value === event.target.value);

      config.template_json['validationChoicePattern'] = config.template_json['validationChoicePattern'][0];
    }
    else if (event.target.type === 'text' && event.target.className === 'sp-pattern form-control') {
      let mainIndex = ['custom', 'email', 'url', 'date(us)', 'date(european)', '12htimeexpression', '24htimeexpression', 'us phone number', 'us zip code'].indexOf(config.template_json['validationChoicePattern'].value);

      config.template_json['validationChoicePattern'].controltypes[event.target.id].configuredValues = event.target.value;
      config.template_json.dropdownoptions[mainIndex] = config.template_json['validationChoicePattern'];
    }
    else if (event.target.type === 'text' && ['predefined-values form-control', 'prohibited-pattern form-control', 'date-time-control form-control'].indexOf(event.target.className) !== -1) {
      let mainIndex = event.target.id;
      config.template_json.controltypes[mainIndex].configuredValues = event.target.value;
    }
    else if (event.target.id === 'validationJSONLength') {
      config.template_json['dropdownoptions'] = JSON.parse(config.template_json['resetDropdownOptions']);
      config.template_json['validationJSONLength'] = config.template_json['dropdownoptions'].filter((validation, index) => validation.value.toLowerCase() === event.target.value.toLowerCase());

      config.template_json['validationJSONLength'] = config.template_json['validationJSONLength'][0];
    }
    else if (event.target.type === 'checkbox' && event.target.className === 'file-type form-check-input') {
      config.template_json.controltypes[event.target.id].selected = event.target.checked;
    }
    else if (event.target.type === 'date') {

      let parentId = event.target.getAttribute('data-parent-id');
      let dateControl = config.template_json['controltypes'][parentId].controltype[event.target.id];
      dateControl.configuredValues = event.target.value;
    }
    else if (event.target.type === 'time') {

      let parentId = event.target.getAttribute('data-parent-id');
      let timeControl = config.template_json['controltypes'][parentId].controltype[event.target.id];
      timeControl.configuredValues = event.target.value;
    }
    else if (event.target.id === 'validationNumberLength') {
      config.template_json['dropdownoptions'] = JSON.parse(config.template_json['resetDropdownOptions']);

      config.template_json['validationNumberLength'] = config.template_json['dropdownoptions'].filter((validation, index) => validation.value === event.target.value);

      config.template_json['validationNumberLength'] = config.template_json['validationNumberLength'][0];
    }
    //this.setState({ config: config })
    this.props.handleFormInput(config);
  }

  renderFieldValidation(validationConfig) {
    let title = validationConfig.title.toLowerCase();
    let validationTemplate;

    switch (title) {
      case 'required field':
        validationTemplate = <FormGroup>
          <Label>
            <span className="text-capitalize">{validationConfig['template_json'].customerrormessage['title']}</span>
          </Label>
          <Input
            type={validationConfig['template_json'].customerrormessage['controltype']}
            value={validationConfig.template_json['customerrormessage'].defaultvalidationmessage}
            onChange={(event) => this.handleInput(event)} />
        </FormGroup>;
        break;
      case 'unique field':
        validationTemplate = <FormGroup>
          <Label>
            <span className="text-capitalize">{validationConfig['template_json'].customerrormessage['title']}</span>
          </Label>
          <Input
            type={validationConfig['template_json'].customerrormessage['controltype']}
            value={validationConfig.template_json['customerrormessage'].defaultvalidationmessage}
            onChange={(event) => this.handleInput(event)} />
        </FormGroup>;
        break;
      case 'limit character count':
        validationTemplate = <React.Fragment>
          <FormGroup>
            <Label>
              <span className="text-capitalize">{validationConfig['template_json'].customerrormessage['title']}</span>
            </Label>
            <Input
              type={validationConfig['template_json'].customerrormessage['controltype']}
              value={validationConfig.template_json['customerrormessage'].defaultvalidationmessage}
              onChange={(event) => this.handleInput(event)} />
          </FormGroup>
          <FormGroup>
            <Row>
              <Col md={4}>
                <Input type="select" className="text-capitalize" name="validationChoiceLength" id="validationChoiceLength" onChange={(event) => this.handleInput(event)} defaultValue="between" ref={this.selectRef}>
                  {
                    validationConfig['template_json'].dropdownoptions.map((dropValue, index) =>
                      <option vlaue={dropValue.value} key={dropValue.id} >{dropValue.value}</option>
                    )
                  }
                </Input>
              </Col>
              {
                !validationConfig.template_json['validationChoiceLength'] ?
                  validationConfig['template_json'].dropdownoptions.forEach((dropValue, index) => {
                    if (dropValue.value === 'between') {
                      validationConfig.template_json['validationChoiceLength'] = dropValue
                    }
                  }) : ''
              }
              {
                validationConfig.template_json['validationChoiceLength'].controltypes.map((cltrValue, index) =>
                  <Col md={4} key={'col_' + index}>
                    <Input type={'number'} placeholder={cltrValue.title} min={1} id={index} value={cltrValue.configuredValues} key={index} onKeyDown={() => { return false }} onChange={(event) => this.handleInput(event)} />
                  </Col>)
              }
            </Row>
          </FormGroup>
        </React.Fragment>;
        break;
      case "match a specific pattern":
        validationTemplate = <React.Fragment>
          <FormGroup>
            <Label>
              <span className="text-capitalize">{validationConfig['template_json'].customerrormessage['title']}</span>
            </Label>
            <Input
              type={validationConfig['template_json'].customerrormessage['controltype']}
              value={validationConfig.template_json['customerrormessage'].defaultvalidationmessage}
              onChange={(event) => this.handleInput(event)} />
          </FormGroup>
          <FormGroup>
            <Row>
              <Col md={4}>
                <Input type="select" className="text-capitalize" name="validationChoicePattern" id="validationChoicePattern" onChange={(event) => this.handleInput(event)} defaultValue="custom">
                  {
                    validationConfig['template_json'].dropdownoptions.map((dropValue, index) =>
                      <option vlaue={dropValue.value} key={dropValue.id} >{dropValue.value}</option>
                    )
                  }
                </Input>
              </Col>
              {
                !validationConfig.template_json['validationChoicePattern'] ?
                  validationConfig['template_json'].dropdownoptions.forEach((dropValue, index) => {
                    if (dropValue.value === 'custom') {
                      validationConfig.template_json['validationChoicePattern'] = dropValue
                    }
                  }) : ''
              }
              {
                validationConfig.template_json['validationChoicePattern'].controltypes.map((cltrValue, index) =>
                  <Col md={4} key={'col_' + index}>
                    <Input type={'text'} placeholder={cltrValue.title} min={1} id={index} value={cltrValue.configuredValues} key={index}
                      className="sp-pattern"
                      onChange={(event) => this.handleInput(event)} />
                  </Col>)
              }
            </Row>
          </FormGroup>
        </React.Fragment>;
        break;
      case 'accept only specified values':
        validationTemplate = <React.Fragment>
          <FormGroup>
            <Label>
              <span className="text-capitalize">{validationConfig['template_json'].customerrormessage['title']}</span>
            </Label>
            <Input
              type={validationConfig['template_json'].customerrormessage['controltype']}
              value={validationConfig.template_json['customerrormessage'].defaultvalidationmessage}
              onChange={(event) => this.handleInput(event)} />
          </FormGroup>
          <FormGroup>
            <Row>
              {
                validationConfig.template_json['controltypes'].map((cltrValue, index) =>
                  <Col md={6} key={'col_' + index}>
                    <Input type={'text'} placeholder={cltrValue.title} min={1} id={index} value={cltrValue.configuredValues} key={index}
                      className="predefined-values"
                      onChange={(event) => this.handleInput(event)} />
                  </Col>)
              }
            </Row>
          </FormGroup>
        </React.Fragment>;
        break;
      case "prohibit a specific pattern":
        validationTemplate = <React.Fragment>
          <FormGroup>
            <Label>
              <span className="text-capitalize">{validationConfig['template_json'].customerrormessage['title']}</span>
            </Label>
            <Input
              type={validationConfig['template_json'].customerrormessage['controltype']}
              value={validationConfig.template_json['customerrormessage'].defaultvalidationmessage}
              onChange={(event) => this.handleInput(event)} />
          </FormGroup>
          <FormGroup>
            <Row>
              {
                validationConfig.template_json['controltypes'].map((cltrValue, index) =>
                  <Col md={6} key={'col_' + index}>
                    <Input type={'text'} placeholder={cltrValue.title} min={1} id={index} value={cltrValue.configuredValues} key={index}
                      className="prohibited-pattern"
                      onChange={(event) => this.handleInput(event)} />
                  </Col>)
              }
            </Row>
          </FormGroup>
        </React.Fragment>;
        break;
      case 'limit number of properties':
        validationTemplate = <React.Fragment>
          <FormGroup>
            <Label>
              <span className="text-capitalize">{validationConfig['template_json'].customerrormessage['title']}</span>
            </Label>
            <Input
              type={validationConfig['template_json'].customerrormessage['controltype']}
              value={validationConfig.template_json['customerrormessage'].defaultvalidationmessage}
              onChange={(event) => this.handleInput(event)} />
          </FormGroup>
          <FormGroup>
            <Row>
              <Col md={4}>
                <Input type="select" className="text-capitalize" name="validationJSONLength" id="validationJSONLength" onChange={(event) => this.handleInput(event)} defaultValue="between" ref={this.selectRef}>
                  {
                    validationConfig['template_json'].dropdownoptions.map((dropValue, index) =>
                      <option vlaue={dropValue.value} key={dropValue.id} >{dropValue.value}</option>
                    )
                  }
                </Input>
              </Col>
              {
                !validationConfig.template_json['validationJSONLength'] ?
                  validationConfig['template_json'].dropdownoptions.forEach((dropValue, index) => {
                    if (dropValue.value.toLowerCase() === 'between') {
                      validationConfig.template_json['validationJSONLength'] = dropValue
                    }
                  }) : ''
              }
              {
                validationConfig.template_json['validationJSONLength'].controltypes.map((cltrValue, index) =>
                  <Col md={4} key={'col_' + index}>
                    <Input type={'number'} placeholder={cltrValue.title} min={1} id={index} value={cltrValue.configuredValues} key={index} onKeyDown={() => { return false }} onChange={(event) => this.handleInput(event)} />
                  </Col>)
              }
            </Row>
          </FormGroup>
        </React.Fragment>;
        break
      case "accept only specified file types":
        validationTemplate = <React.Fragment>
          <FormGroup>
            <Label>
              <span className="text-capitalize">{validationConfig['template_json'].customerrormessage['title']}</span>
            </Label>
            <Input
              type={validationConfig['template_json'].customerrormessage['controltype']}
              value={validationConfig.template_json['customerrormessage'].defaultvalidationmessage}
              onChange={(event) => this.handleInput(event)} />
          </FormGroup>
          <FormGroup>
            <Row>
              {
                validationConfig.template_json['controltypes'].map((cltrValue, index) =>
                  <Col md={6} key={'col_' + index}>
                    <FormGroup check>
                      <Label check>
                        <Input type={cltrValue.uicontroltype} id={index} value={cltrValue.configuredValues} key={index}
                          checked={cltrValue.selected}
                          className="file-type"
                          onChange={(event) => this.handleInput(event)} />
                        {' ' + cltrValue.title}
                      </Label>
                    </FormGroup>
                  </Col>)
              }
            </Row>
          </FormGroup>
        </React.Fragment>;
        break;
      case 'accept only specified image dimensions':
        validationTemplate = <React.Fragment>
          <Row>
            {
              validationConfig.template_json['controltypes'].map((cltrValue, index) =>
                <Col md={6} key={'col_min_max_' + index}>
                  <FormGroup>
                    <Input type="select" className="text-capitalize" onChange={(event) => this.handleInput(event)} defaultValue="exactly" ref={this.selectRef}>
                      {
                        cltrValue.dropdownoptions.map((dropdnValue, index) =>
                          <option vlaue={dropdnValue.value} key={dropdnValue.id}>{dropdnValue.value}
                          </option>
                        )
                      }
                    </Input>
                  </FormGroup>
                </Col>)
            }
          </Row>
        </React.Fragment>;
        break;
      case "accept only specified date range":
        validationTemplate = <React.Fragment>
          {
            validationConfig.template_json['controltypes'].map((dpValue, parentIndex) =>
              <Row key={"row_" + parentIndex}>
                <Label>
                  <span className="text-capitalize">{dpValue['title']}</span>
                </Label>
                {
                  dpValue.controltype.map((dateNtimeValue, index) =>
                    <Col md={3} key={'date_row_' + index}>
                      <FormGroup>
                        <Input
                          key={'date_control_' + index}
                          className="date-time-control"
                          id={index}
                          data-parent-id={parentIndex}
                          type={dateNtimeValue.uicontroltype}
                          value={dateNtimeValue.configuredValues}
                          onChange={(event) => this.handleInput(event)} />
                      </FormGroup>
                    </Col>
                  )
                }
              </Row>
            )
          }
        </React.Fragment>
        break;
      case 'accept only specified number range':
        validationTemplate = <React.Fragment>
          <FormGroup>
            <Label>
              <span className="text-capitalize">{validationConfig['template_json'].customerrormessage['title']}</span>
            </Label>
            <Input
              type={validationConfig['template_json'].customerrormessage['controltype']}
              value={validationConfig.template_json['customerrormessage'].defaultvalidationmessage}
              onChange={(event) => this.handleInput(event)} />
          </FormGroup>
          <FormGroup>
            <Row>
              <Col md={4}>
                <Input type="select" className="text-capitalize" name="validationNumberLength" id="validationNumberLength" onChange={(event) => this.handleInput(event)} defaultValue="between">
                  {
                    validationConfig['template_json'].dropdownoptions.map((dropValue, index) =>
                      <option vlaue={dropValue.value} key={dropValue.id} >{dropValue.value}</option>
                    )
                  }
                </Input>
              </Col>
              {
                !validationConfig.template_json['validationNumberLength'] ?
                  validationConfig['template_json'].dropdownoptions.forEach((dropValue, index) => {
                    if (dropValue.value.toLowerCase() === 'between') {
                      validationConfig.template_json['validationNumberLength'] = dropValue
                    }
                  }) : ''
              }
              {
                validationConfig.template_json['validationNumberLength'].controltypes.map((cltrValue, index) =>
                  <Col md={4} key={'col_' + index}>
                    <Input type={'number'} placeholder={cltrValue.title} min={1} id={index} value={cltrValue.configuredValues} key={index} onKeyDown={() => { return false }} onChange={(event) => this.handleInput(event)} />
                  </Col>)
              }
            </Row>
          </FormGroup>
        </React.Fragment>;
        break;
      default:
        validationTemplate = "";
    }
    return validationTemplate;
  }

  render() {
    let tempSetting = this.props.config;
    return (
      tempSetting !== undefined && tempSetting.type === 'setting' ?
        <FormGroup check>
          <Label check>
            <Input type={tempSetting.uicontrol_type}
              key={tempSetting.fieldtype_setting_sid}
              checked={tempSetting.checked ? true : false}
              onChange={(event) => { this.handleInput(event) }} />{' ' + tempSetting.execution_json.title}
          </Label>
          <p className="text-secondary">{tempSetting.execution_json.description}</p>
        </FormGroup>
        :
        tempSetting !== undefined && tempSetting.type === 'validation' && !tempSetting.checked ?
          <FormGroup check>
            <Label check>
              <Input type={tempSetting.uicontrol_type}
                key={tempSetting.fieldtype_setting_sid}
                checked={tempSetting.checked ? true : false}
                onChange={(event) => { this.handleInput(event) }} />{' ' + tempSetting.title}
            </Label>
            <p className="text-secondary">{tempSetting.template_json.description}</p>
          </FormGroup>
          :
          <React.Fragment>
            <FormGroup check>
              <Label check>
                <Input type={tempSetting.uicontrol_type}
                  key={tempSetting.fieldtype_setting_sid}
                  checked={tempSetting.checked}
                  onChange={(event) => { this.handleInput(event) }} />{' ' + tempSetting.title}
              </Label>
              <p className="text-secondary">{tempSetting.template_json.description}</p>
            </FormGroup>
            {
              this.renderFieldValidation(tempSetting)
            }
          </React.Fragment>);
  }
}

export default connect((state) => {
  return { isLoader: state.isLoader }
})(FormValidationControl);

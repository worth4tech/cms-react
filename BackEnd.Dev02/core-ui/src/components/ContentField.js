import React from 'react';
import { Input, FormGroup, Label } from 'reactstrap';

class ContentField extends React.PureComponent {
  constructor(props) {
    super(props);
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(event) {
    let fieldConf = this.props.fieldConfiguation;
    fieldConf.value = event.target.value;
    this.props.handleFormInput(fieldConf);
  }
  render() {

    let field = this.props.fieldConfiguation;
    
    return (
      <React.Fragment>
        <FormGroup>
          <Label>{field.name}
            {
              field.required ? <span className="invalid-feedback"> *</span> : ''
            }
          </Label>

          <Input
            type={field.uicontroltype}
            onChange={this.handleInput}
            defaultValue={field.value}
            id={field.index}
            required={field.required.isRequired}
            minLength={field.range && field.range.isRange && field.range.min ? field.range.min : ''}
            maxLength={field.range && field.range.isRange && field.range.max ? field.range.max : ''}
            className={
              field.message &&
                (
                  (field.required && (!field.value || field.value === '')) ||

                  (field.range && (field.value && field.value !== '') && field.value.length < field.range.min) ||

                  (field.pattern && (field.value && field.value !== '') && field.value.match(field.value)) ||

                  (field.specificItems && (field.value && field.value !== '') &&
                    field.specificItems.predefinedlist.indexOf(field.value) === -1) ||

                  (field.preventSpecific && (field.value && field.value !== '') &&
                    field.value.match(field.preventSpecific.regex))
                )
                ? 'is-invalid' : ''} />
          <div className="text-secondary">{field.description}</div>
          {
            field.message &&
              (
                (field.required && (!field.value || field.value === '')) ||

                (field.range && (field.value && field.value !== '') && field.value.length < field.range.min) ||

                (field.pattern && (field.value && field.value !== '') && !field.value.match(field.pattern.regex)) ||

                (field.specificItems && (field.value && field.value !== '') &&
                  field.specificItems.predefinedlist.indexOf(field.value) === -1) ||

                (field.preventSpecific && (field.value && field.value !== '') &&
                  field.value.match(field.preventSpecific.regex))
              )
              ?
              <div className="invalid-feedback">{field.message}</div>
              : ''
          }

        </FormGroup>
      </React.Fragment>
    );
  }
}

export default ContentField;
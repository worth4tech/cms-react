import { createStore } from 'redux';
import reducer from './reducer';
const initialState = {
    isLogin: false,
    isLoader: false
};
const store = createStore(reducer, initialState)
export default store;
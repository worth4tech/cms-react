const reducer = (state, action) => {
  switch (action.type) {
    case "IS_LOGIN":
      return state = {
        ...state,
        isLogin: action.payload
      }

    case "TOGGLE_LOADER":
      return state = {
        ...state,
        isLoader: action.payload
      }
    case "LOG_OUT":
      let contentType = localStorage.getItem('content_type');
      localStorage.clear();
      localStorage.setItem('content_type', contentType);
      return state = {
        ...state,
        isLogin: action.payload
      }
    default:
      return state;
  }
}
export default reducer;

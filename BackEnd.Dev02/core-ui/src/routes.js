import React from 'react';

const ContentType = React.lazy(() => import('./views/ContentType/ContentType'));
const ContentTypeDetail = React.lazy(() => import('./views/ContentType/ContentTypeDetail'));
const Content = React.lazy(() => import('./views/Content/Content'));
const ContentEntry = React.lazy(() => import('./views/Content/ContentEntry'));
const Assets = React.lazy(() => import('./views/Assets/Assets'));
const AssetEntry = React.lazy(() => import('./views/Assets/AssetEntry'));
const Users = React.lazy(() => import('./views/Users/Users'));
const User = React.lazy(() => import('./views/Users/User'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/content-type', name: 'ContentType', exact: true, component: ContentType },
  { path: '/content-type/:contenttype_sid/detail', exact: true, name: 'ContentTypeDetail', component: ContentTypeDetail },

  { path: '/content-entries', name: 'Content', exact: true, component: Content },
  { path: '/:content_type_sid/content-entries/:entry_sid/detail', name: 'ContentEntry', exact: true, component: ContentEntry },
  { path: '/assets', name: 'Assets', exact: true, component: Assets },
  { path: '/assets/:asset_sid/detail', name: 'Assets', exact: true, component: AssetEntry },
  { path: '/users', exact: true, name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
];
export default routes;

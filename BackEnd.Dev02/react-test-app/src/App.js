import React, { Component } from 'react';
import { HashRouter, BrowserRouter, Route, Switch, } from 'react-router-dom';
import { Dashboard, Login } from './pages';
import PrivateRoute from './components/PrivateRoute.jsx';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = { message: "" };
  }

  render() {
    return (
      <BrowserRouter history={history}>
        <Switch>
          <Route exact path="/" name="Login Page" component={Login} />
          <Route exact path="/login" name="Login Page" component={Login} />
          <PrivateRoute path="/" name="Dashboard Page" component={Dashboard} />
        </Switch>
      </BrowserRouter>
    );
  }
}


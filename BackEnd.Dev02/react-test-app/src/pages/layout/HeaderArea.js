import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button, Card, Row, Col } from 'react-bootstrap'

class HeaderArea extends Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.handleLogOut = this.handleLogOut.bind(this);
  }
  handleLogOut() {
    localStorage.clear();
  }
  render() {
    return (
      <Row>
        <Col xs={10} md={10}>
          <h3>This is Header area</h3>
        </Col>
        <Col xs={2} md={2}>
          <span className="text-right">
            <Link className="btn btn-success mb-2" onClick={this.handleLogOut} to="/login">Sign Out</Link >
          </span>
        </Col>
      </Row>
    );
  }
}
export default HeaderArea;

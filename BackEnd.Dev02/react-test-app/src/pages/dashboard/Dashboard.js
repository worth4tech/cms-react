import React, { Component } from 'react';
import { HeaderArea, SidebarArea } from '../layout'
import RemoveButton from './RemoveButton';
import { AgGridReact, GridOptions } from 'ag-grid-react';
import APIService from '../../service/APIService';

export default class Dashboard extends Component {

  constructor(props) {

    super(props);
    this.state = {
      columnDefs: [{
        headerName: "Make", field: "make", sortable: true, filter: true
      }, {
        headerName: "Model", field: "model", sortable: true, filter: true
      }, {
        headerName: "Price", field: "price", sortable: true, filter: true
      },
      {
        headerName: "Action", field: "", cellRenderer: "actionRenderer",
      }],
      rowData: [{
        make: "Toyota", model: "Celica", price: 35000
      }, {
        make: "Ford", model: "Mondeo", price: 32000
      }, {
        make: "Porsche", model: "Boxter", price: 72000
      }],
      gridOptions: {
        pagination: true,
        paginationPageSize: 1,
        currentPage: 1
      },
      gridApi: {},
      context: { componentParent: this },
      frameworkComponents: {
        actionRenderer: RemoveButton
      }
    };
    this.onGridReady = this.onGridReady.bind(this);
    this.onPaginationChanged = this.onPaginationChanged.bind(this);
  }

  componentDidMount() {

  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.setState({
      "gridApi": params.api
    })

    let url = 'RCMS.Data/v1/Accounts/AC3B6907DB-97A6-4FC2-AFFC-48297A6FC827/ContentTypes?SearchText&Page=1&PageSize=10&SortColumn&SortOrder&Filters&FiltersList'

    APIService.get(url).then((success) => {
      console.log(success);
    }).catch(error => {
      console.error(error.response)
    })
  }

  onPaginationChanged() {
    let pagination = this.state.gridApi.paginationProxy
    if (pagination != undefined) {
      console.log(pagination.currentPage);
    }
  }
  methodFromParent(cell) {
    console.log(cell)
  }
  render() {
    return (
      <div className="container">
        <HeaderArea />
        <SidebarArea />
        <div className="ag-theme-balham"
          style={{
            height: '500px',
            width: '100%'
          }}
        >
          <AgGridReact
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}
            pagination={true}
            context={this.state.context}
            gridOptions={this.state.gridOptions}
            onGridReady={this.onGridReady}
            onPaginationChanged={this.onPaginationChanged}
            frameworkComponents={this.state.frameworkComponents}
          >
          </AgGridReact>
        </div>

      </div >
    );
  }
}


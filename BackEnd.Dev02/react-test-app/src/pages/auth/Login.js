import React, { Component } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import { ToastContainer, toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';
import { FormValidation } from "calidation";
import axios from 'axios';

class Login extends Component {

  constructor(props) {
    //console.log(process.env.NODE_ENV)
    super(props);
    this.state = {
      user: {
        grant_type: "password",
        emailaddress: "",
        password: ""
      },
    };
    this.handleSignin = this.handleSignin.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSignin(event) {
    let user = this.state.user;

    if (event.target.name == 'emailaddress') {
      user.emailaddress = event.target.value;
      this.setState({ user: user });
    }
    else {
      user.password = event.target.value;
      this.setState({
        user: user
      })
    }
  }
  handleSubmit({ fields, errors, isValid }) {
    if (isValid) {
      axios.post('http://localhost:3000/api/user/Auth.Data/v1/Accounts/Auth', this.state.user, {
        headers: {
          'content-type': 'application/json'
        }
      }).
        then((success) => {
          let successResult = success.data;
          localStorage.setItem('access_token', successResult.result.access_Token);
          localStorage.setItem('user_sid', successResult.result.user_sid);
          localStorage.setItem('refresh_Token', successResult.result.refresh_Token);
          localStorage.setItem('first_name', successResult.result.first_name);
          localStorage.setItem('last_name', successResult.result.last_name);
          localStorage.setItem('email_address', successResult.result.email_address);
          this.props.history.push('/dashboard');

        }).catch((error) => {
          let errorResult = error.response;
          toast.error(errorResult.data.message, {
            position: "top-left",
            autoClose: "5000",
            hideProgressBar: "true",
            closeOnClick: "true",
            pauseOnHover: "true",
            draggable: "true"
          });
        })
    }
  }

  render() {
    let user = this.state.user;

    const config = {
      emailaddress: {
        isRequired: "Email is required!",
        isEmail: "Valid emails only, please!"
      },
      password: {
        isRequired: "Password field required!",
        isMinLength: {
          message: "8 Character at least for password",
          length: 8
        }
      }
    };

    if (localStorage.getItem('access_token')) {
      return <Redirect to='/dashboard' />;
    }
    return (
      <div className="container">
        <ToastContainer position="top-left">
        </ToastContainer>
        <Row>
          <Col md={3}></Col>
          <Col xs={6} md={6}>
            <div className="sigin-form" noValidate>
              <h5 className="text-center mt-3">Sign In</h5>
              <FormValidation onSubmit={this.handleSubmit} method="post" noValidate config={config}>
                {({ fields, errors, submitted }) => (
                  <React.Fragment>
                    <Form.Group >
                      <Form.Label>Email:<span className="text-danger">*</span></Form.Label>
                      <Form.Control type="email" placeholder="Enter email" id="emailaddress" name="emailaddress" value={user.emailaddress} onChange={this.handleSignin} ref="emailaddress" />
                      {submitted && errors.emailaddress &&
                        <div className="text-danger">{errors.emailaddress}</div>
                      }
                    </Form.Group>

                    <Form.Group >
                      <Form.Label>Password:<span className="text-danger">*</span> </Form.Label>
                      <Form.Control type="password" placeholder="Enter password" name="password" value={user.password} onChange={this.handleSignin} id="password" ref="password" />
                      {submitted && errors.password &&
                        <div className="text-danger">{errors.password}</div>
                      }
                    </Form.Group>

                    <Button variant="primary" type="submit">
                      Submit
                </Button>
                  </React.Fragment>
                )}
              </FormValidation>
            </div>
          </Col>
        </Row>
      </div >
    );
  }
}
export default Login;

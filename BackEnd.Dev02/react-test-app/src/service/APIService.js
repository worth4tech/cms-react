import axios from 'axios';

let API_URL = ''
if (process.env.NODE_ENV == 'development') {
    API_URL = 'http://localhost:3000/api/other/'
}
else {
    //URL for production
}
let headers = {
    'content-type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
}
export default axios.create({
    baseURL: API_URL,
    headers: headers
});